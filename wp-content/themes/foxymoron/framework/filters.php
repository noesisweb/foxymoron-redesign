<?php
function filter_query($query) {
    if (!is_admin() && is_post_type_archive('news') && $query->is_main_query()) {
        $query->set('posts_per_page', -1);
        $tax_query = array(
                array(
	                'taxonomy' => 'news-category',
	                'field' => 'slug',
	                'terms' => 'news_cat'
            )
        );
        $query->set('tax_query', $tax_query);
    }
}
add_action('pre_get_posts', 'filter_query');

add_filter('post_gallery','customFormatGallery',10,2);

add_filter( 'gform_pre_render', 'populate_posts' );

function populate_posts( $form ) {
	
	foreach ( $form['fields'] as &$field ) {
		
        if ( $field->type != 'select' ) {
            continue;
        }
     
       
        $choices = array();
        $taxonomy_names = wp_get_post_terms( get_the_ID(), 'cities' );
        $is_selected = false;
        if(!empty($_GET['career_city'])) {
	    	$career_city = $_GET['career_city'];    
        } else {
	        $career_city = '';
        }
        
       
        foreach ($taxonomy_names as $term  ) {
	        if($career_city == $term->slug)
            	$choices[] = array( 'text' => $term->name, 'value' => $term->slug,'isSelected' => true );
            else
            	$choices[] = array( 'text' => $term->name, 'value' => $term->slug,'isSelected' => false ); 
        }
 
        $field->placeholder = 'Select City';
        $field->choices = $choices;
 
    }
	return $form;
}

add_filter('body_class', 'multisite_body_classes');

function multisite_body_classes($classes) {
        $id = get_current_blog_id();
        $slug = strtolower(str_replace(' ', '-', trim(get_bloginfo('name'))));
        $classes[] = $slug;
        if (is_front_page() || is_page('methodology') || is_page('home') || is_page('contact-us') || is_post_type_archive('contact_us') || is_page('services') || is_page('careers') || is_post_type_archive('news') || is_post_type_archive('career') || is_singular( 'news' )) {
	        $classes[] = 'background-dark';
        } else if (is_page('clients')) {
	        $classes[] = 'background-light';
        } else if (is_page('team') || is_post_type_archive('work')) {
	        $classes[] = 'background-none';
	        $classes[] = 'background-change';  
        } 
        return $classes;
}