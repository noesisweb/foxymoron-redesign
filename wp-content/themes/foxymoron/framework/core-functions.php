<?php
function customFormatGallery($string,$attr) {
    $posts_order_string = $attr['ids'];
    $posts_order = explode(',', $posts_order_string);
	$output = '';
	$output .= '<div class="foxy-banner case-study-banner2">';
	$output .= '<div class="container">';
	$output .= '<div class="swiper-container">';
	$output .= '<div class="swiper-wrapper">';
	    
    $posts = get_posts(array(
          'include'   => $posts_order,
          'post_type' => 'attachment', 
          'orderby'   => 'asc'
    ));

    foreach($posts as $imagePost){
        $output .= '<div class="swiper-slide"><img src="' . wp_get_attachment_image_src($imagePost->ID, 'full')[0] . '" alt="case-studys" /></div>';
    }
    $output .= '</div>';
    $output .= '<div class="swiper-pagination"></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    
    return $output;
}

function embed_code_callback( $atts ) {
	$embed_codes = get_field('embed_code','option');
	$output = '';
	$output .= '<div class="foxy-banner case-study-banner3">';
	$output .= '<div class="container">';
	$output .= '<div class="swiper-container">';
	$output .= '<div class="swiper-wrapper">';
	foreach($embed_codes as $code) {
		$output .= '<div class="swiper-slide">';
		$output .= '<div class="col-md-4">';
		$output .= $code['embed_code_text'];
		$output .= '</div>';
		$output .= '</div>';	
	}
	$output .= '</div>';
	$output .= '<div class="swiper-pagination"></div>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '</div>';
	return $output;
}

add_shortcode( 'embed_code', 'embed_code_callback' );

function get_category_featured_post_ids() { 
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 3,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'News',
	);
	$post_args['meta_query'] = array(
	    'relation' => 'AND',
	    array(
	      'key'     => 'is_on_slide',
	      'value'   => 'yes',
	      'compare' => '=',
	    ),
	  );
	$featured_posts = new WP_Query( $post_args );
	$post_ids = array();
	if($featured_posts->have_posts()) {
		while($featured_posts->have_posts()) {
			$featured_posts->the_post();
			$post_ids[] = get_the_ID();
		}
		wp_reset_postdata();
	}
    $post_ids = array_unique($post_ids);
    return $post_ids;
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'options-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function getNewsData($post_id) {
	
	$main_cat = get_the_category($post_id);
	$parent_cat = '';
	$primary_child_cat = '';
	$child_category = '';
	$primary_cat_id = '';
	
	$primary_cat = new WPSEO_Primary_Term('category', $post_id); 
	$primary_cat = $primary_cat->get_primary_term(); 
		
	foreach($main_cat as $category) {
		if($category->parent == 0) {
			$parent_cat = $category->name; 
		}	
		if($category->term_id == $primary_cat && $category->parent != 0) {
			$primary_cat_id = $category->term_id;
			$primary_child_cat = $category->name;
		} else {
			$child_category = $category->name;
		}
	}
	$data = array();
	$data['parent_cat'] = $parent_cat;
	$data['primary_child_cat'] = $primary_child_cat;
	$data['child_category'] = $child_category;
	return $data;
}
function get_filter_data($dept_slug,$city_slug) {
	$args = array(
		'post_type' => 'career',
		'tax_query' => array(
		    array(
		        'taxonomy' => 'cities',
		        'field'    => 'slug',
		        'terms'    => $city_slug,
		    ),
		    array(
		        'taxonomy' => 'departments',
		        'field'    => 'slug',
		        'terms'    => $dept_slug,
		        ),
		    ),
		);
		$query = new WP_Query( $args );
		return $query->posts;
}
	
function get_all_data($dept_slug, $no_of_post = -1,$exclude_post_id = '') {
	$args = array(
		'post_type' => 'career',
		'post__not_in' => array($exclude_post_id),
		'post_status'=>'publish',
		'posts_per_page'=>$no_of_post,
		'order_by'=>'date',
		'order'=>'DESC',
		    
		'tax_query' => array(
		    array(
		        'taxonomy' => 'departments',
		        'field'    => 'slug',
		        'terms'    => $dept_slug,
		    ),
		),
	);
	$query = new WP_Query( $args );
	return $query->posts;
}

// Redesigned website
/**
 * Get post thumbnail.
 *
 * This function gets post thumbnail.
 */
function get_post_thumbnail($post_id, $size = 'medium') {
    $image_html = '';
    $thumbnail_id = get_post_thumbnail_id($post_id);
    if ($thumbnail_id) {
        $image_html = wp_get_attachment_image($thumbnail_id, $size);
    }
    return $image_html;
}

/**
 * Get post thumbnail URL.
 *
 * This function gets post thumbnail URL.
 */
function get_post_thumbnail_url($post_id, $size = 'medium') {
    $image_url = '';
    $thumbnail_id = get_post_thumbnail_id($post_id);
    if ($thumbnail_id) {
        $image_url = wp_get_attachment_image_url($thumbnail_id, $size);
    }
    return $image_url;
}

/**
* Get Primary Taxonomy term
* 
* This is function retrive primary term name from taxonomy
*/

if ( ! function_exists( 'get_primary_taxonomy_id' ) ) {
	function get_primary_taxonomy_id( $post_id, $taxonomy ) {
	    $prm_term = '';
	    if (class_exists('WPSEO_Primary_Term')) {
	        $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post_id );
	        $prm_term = $wpseo_primary_term->get_primary_term();
	    }
	    if ( !is_object($wpseo_primary_term) && empty( $prm_term ) ) {
	        $term = wp_get_post_terms( $post_id, $taxonomy );
	        if (isset( $term ) && !empty( $term ) ) {
	            return wp_get_post_terms( $post_id, $taxonomy )[0]->term_id;
	        } else {
	            return '';
	        }
	    }
	    return $wpseo_primary_term->get_primary_term();
	}
}