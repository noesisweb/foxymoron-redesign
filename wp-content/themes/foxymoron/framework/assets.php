<?php
/**
 * Enqueue scripts and styles.
 */
function foxymoron_scripts() {
	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.min.css', true);
    wp_enqueue_style('swiperslider-css', get_template_directory_uri() . '/assets/css/swiper.min.css', true);
	wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/assets/css/font-awesome.min.css', true);
	wp_enqueue_style('icomoon-css', get_template_directory_uri() . '/assets/css/icomoon.css', true);
	wp_enqueue_style('pagepiling-css', get_template_directory_uri() . '/assets/css/jquery.fullPage.css', true);
	wp_enqueue_style('colorify-css', get_template_directory_uri() . '/assets/css/colorify.css', true);

	wp_enqueue_style( 'foxymoron-style', get_stylesheet_uri() );
	//wp_enqueue_script('foxy-jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '20151215', false);
	wp_enqueue_script('foxy-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '20151215', false);
	wp_enqueue_script('foxy-dragscroll', get_template_directory_uri() . '/assets/js/dragscroll.js', array('jquery'), '20151215', false);
	wp_enqueue_script('foxy-swiper', get_template_directory_uri() . '/assets/js/swiper.min.js', array('jquery'), '20151215', false);
	wp_enqueue_script('foxy-isotope', get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js', array('jquery'), '20151215', false);

	wp_enqueue_script('foxy-fullpage', get_template_directory_uri() . '/assets/js/jquery.fullPage.js', array('jquery'), '20151215', false);
	wp_enqueue_script('foxy-parsley', get_template_directory_uri() . '/assets/js/parsley.min.js', array('jquery'), '20151215', false);
	wp_enqueue_script('foxy-mousewheel', get_template_directory_uri() . '/assets/js/jquery.mousewheel.min.js', array('jquery'), '20151215', false);

    wp_enqueue_script('foxy-common', get_template_directory_uri() . '/assets/js/common.js', array(), date('Ymd his'), true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'foxymoron_scripts' );
