<?php get_header(); ?>

<?php
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 6,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'Case Studies',
		'paged' => $paged,
	);
	$featured_posts = new WP_Query( $post_args );	
?>

<div class="case-studies">
	<div class="container">	
		<div class="case-study-wrapper clearfix work-wrapper">
			<?php
			if($featured_posts->have_posts()) {
				while($featured_posts->have_posts()) {
				$featured_posts->the_post();
				$cover_image_id = get_field('cover_image');
				
				if (has_post_thumbnail( get_the_ID()) ) {
					if(!wp_is_mobile()){
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'work_posts_desktop' );
					} else {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'common_posts_mobile' );
					}
				} else {
					$image = '';
				}
				?>
				<a href="<?php echo get_permalink(get_the_ID()); ?>">
			        <div class="cb-block case-study-block">
			            <img src="<?php if(!empty($image)) echo $image[0]; else echo get_template_directory_uri().'/assets/images/df-foxy.jpg'; ?>" alt="case-studys">
			            <div class="cs-info info">
			                <h3><?php the_title(); ?></h3>
			                <p><?php echo get_field('short_description'); ?></p>
			                <span class="fa fa-arrow-right arrow-right"></span>
			            </div>
			        </div>
		        </a>
		        <?php
		        }
		        $total_pages = $featured_posts->max_num_pages;
			    if ($total_pages > 1) {
			        $current_page = max(1, get_query_var('paged'));
			        echo '<div class="work_posts_pagination col-md-12">';
			        echo paginate_links(array(
			            'base' => get_pagenum_link(1) . '%_%',
			            'format' => 'page/%#%',
			            'current' => $current_page,
			            'total' => $total_pages,
			            'prev_text'    => __('« prev'),
			            'next_text'    => __('next »'),
			        ));
			        echo '</div>';
			    } 
		    }
		    wp_reset_query();
	        ?>
		</div>	
	</div>
</div>

<!--
<div class="primary-button">
	<a class="btn" href="#" role="button">Load More Case Studies</a>
</div>
-->

<div class="foxy-patnership">
	<div class="container">
		<div class="row">
			<h3> We Partner With</h3>
			<div class="foxy-patner">
				<?php 
					$partner_images = get_field('partner_section', 'option');		
						
					if(!empty($partner_images)) {
						foreach($partner_images as $partner) {
							$image = wp_get_attachment_image($partner['partner_image'], 'our_partner');
					?>
						<div class="patners"><?php echo $image; ?></div>
					<?php
						}
					} 
				?>				
			</div>
		</div>
	</div>
</div>

<?php get_footer();?>
