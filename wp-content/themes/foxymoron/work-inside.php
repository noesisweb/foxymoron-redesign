<?php /* Template Name: Work Inside Page */ ?>
<?php get_header(); ?>

<div class="page-work-inside">
	<div class="section container-small">
		<div class="page-category-name">Case Studies</div>
		<h2><div class="upperline"></div>Motorola's <div class="text-color-primary">Biggest</div> Phone Launch Ever - <div class="text-color-primary">The Z2 Play</div></h2>
		<p>Proin gravida nibh vel velit auctor aliquet. <span class="highlight">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit</span> consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.<span class="highlight"> Sed non mauris vitae erat </span> consequat auctor eu in elit.</p>
		<div class="section-tag text-color-primary"><span class="icon-Awwwards icon"></span> Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin</div>
	</div>
	<div class="highlights-toggle">
		<span class="icon-Pen-1 icon "></span>
		<span class="icon-Pen-2 icon "><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></span>
	</div>
	<div class="page-banner" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/1.png)"></div>
	<div class="section container-small brand-description">
		<div class="brand-logo">
			<img src="<?php echo get_template_directory_uri();?>/assets/img/moto.png" />
		</div>
		<div class="brand-info">
			<p>Proin gravida nibh vel velit <span class="highlight">auctor aliquet</span>. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>
		</div>
	</div>
	<div class=" page-banner" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/2.jpg)"></div>
	<div class="section container-small">
		<h2>Market Share<br> increase of <div  class="text-color-primary">100 basis</div> points. <div  class="text-color-primary">28% in 2014 - 29% in 2015.</div></h2>
		<table>
			<tr>
				<td>
					<div class="bold-text">Impressions </div>
					<div class="calculations text-color-primary"> 632.98 </div>
					<div class="bold-text">Million</div>
					<div class="small-text">March 2015 was the highest ever</div>
				</td>
				<td>
					<div class="bold-text">Records </div>
					<div class="calculations text-color-primary"> 61.69 </div>
					<div class="bold-text">Million</div>
					<div class="small-text">On Digital Alone</div>
				</td>
				<td>
					<div class="bold-text">Highest Sales </div>
					<div class="calculations text-color-primary"> 37 </div>
					<div class="bold-text">Million Ltr Sold</div>
					<div class="small-text">Volume sales month for castrol Activ</div>
				</td>
			</tr>
		</table>
		<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>
	</div>
	
	<div class="page-banner" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/3.jpg)"></div>
	
	<div class="section container-small">
		<h2> Final <div  class="text-color-primary">words</div></h2>
		<div class="final-words">
			<div class="column">
				<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
			</div>
			<div class="column">
				<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit.</p>
			</div>
		</div>
	</div>
	
	<div class="section brand-words">
		
		Consistency is important on large scale design projects.<br>
		Approaching and trying to solve the problems.
	</div>
	
	<div class="section brand-services">
		<div class="service"></div>
		<div class="service">
			<div class="name">Art <div  class="text-color-primary">Direction</div></div>
			<p> -Lorem Ipsum</p>
		</div>
		<div class="service">
			<div class="name">Video <div  class="text-color-primary">Production</div></div>
			<p> -Lorem Ipsum</p>
			<p> -Lorem Ipsum</p>
		</div>
		<div class="service"></div>
	</div>
	
	<div class="section next-previous-project">
		<div class="project" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/batman-superman.jpg)">
			<div class="overlay"></div>
			<div class="direction"><div class="upperline"></div>Previous Project</div>
			<div class="name text-color-primary">Batman V Superman</div>
		</div>
		<div class="project" style="background-image: url(<?php echo get_template_directory_uri();?>/assets/img/bread.jpg)">
			<div class="overlay"></div>
			<div class="direction"><div class="upperline"></div>Next Project</div>
			<div class="name text-color-primary">Flamboyante</div>
		</div>
	</div>
</div>

<?php get_footer();?>