<?php 
	/* Template Name: news-landing */ 
 	get_header();
 	$args = array(
		'posts_per_page'   => 5,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => 'news',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'suppress_filters' => true 
	);
	$posts_array = get_posts( $args );
/*
	echo '<pre>';
	print_r($posts_array);
	echo '</pre>';
*/
?>
<div class="page-news-landing">
	<div class="horizontal-scroll-wrapper">
		<article class="single-block no-image">
			<div class="article-content">
				<div class="content">
					
					<div class="article-title"><div class="upperline"></div><p>Press</br>Release</p></div>
					<p class="article-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p class="article-list"><span>Archived PR list</span></p>
				</div>
			</div>
		</article>
		<!--
<?php foreach ($posts_array as $key => $value) { ?>
		<article class="single-block">
			<div class="background-image image-1">
				<div class="overlay"></div>
			</div>
			
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p><?php echo $key ?></p></div>
					<p><?php echo $value -> post_title ?></p>
					<p>Onboards</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">FoxyMoron!</a>
				</div>
			</div>
		</article>
		<?php } ?>
-->
		<article class="single-block">
			<div class="background-image image-2">
				<div class="overlay"></div>
			</div>
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p>02</p></div>
					<p>FoxyMoron </p>
					<p>Bags</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">Burger King</a>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="background-image image-1">
				<div class="overlay"></div>
			</div>
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p>03</p></div>
					<p>GoAir</p>
					<p>Onboards</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">FoxyMoron!</a>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="background-image image-2">
				<div class="overlay"></div>
			</div>
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p>04</p></div>
					<p>GoAir</p>
					<p>Onboards</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">FoxyMoron!</a>
				</div>
			</div>
		</article>	
		<article class="single-block">
			<div class="background-image image-1">
				<div class="overlay"></div>
			</div>
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p>05</p></div>
					<p>GoAir</p>
					<p>Onboards</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">FoxyMoron!</a>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="background-image image-2">
				<div class="overlay"></div>
			</div>
			<div class="article-content">
				<div class="content">
					<div class="article-number"><p>06</p></div>
					<p>GoAir</p>
					<p>Onboards</p>
					<a class="coloured" href="<?php echo site_url(); ?>/new-inner-screen/">FoxyMoron!</a>
				</div>
			</div>
		</article>	
	</div>
	<div class="horizontal-scroll-navigation">
		<button class="navigation navigation-left"><span class="fa fa-arrow-left"></span></button>
		<button class="navigation navigation-right"><span class="fa fa-arrow-right"></span></button>
	</div>
</div>
<?php get_footer();?>
