<?php /* Template Name: Home Page Static */ ?>
<?php get_header(); ?>

<div class="page-home">
    <div class="section-title active">
        <div class="left"><div>Lorem Ipsum</div></div>
        <div class="right"><div>Lorem Ipsum</div></div>
    </div>
    <div class="section-title">
        <div class="left"><div>Ariel</div></div>
        <div class="right"><div>Ariel</div></div>
    </div>
    <div class="section-title">
        <div class="left"><div>Lorem 3</div></div>
        <div class="right"><div>Lorem 3</div></div>
    </div>
    <div class="section-title">
        <div class="left"><div>Lorem 4</div></div>
        <div class="right"><div>Lorem 4</div></div>
    </div>
    <div class="content">

        <div class="home-content-slider">
            <ul>
                <li class="active">
                    <div class="content-text">
                        <h3>Okay,<br> so here’s a <span class="text-color-primary">*seriously funny*</span> story.</h3>
                        <p> A bunch of friends embark on a tiny gargantuan
                            project. A vocational avocation, if you may. Starting a digital agency of their own. A bit of business, a bit of fun and large amounts of crazy. </p>
                        <p>And within moments, or years (God knows it feels like both!) it snowballed into one of India’s biggest digital outfits. #HumbleBrag That’s the oxymoronic story of Foxymoron. </p>
                        <p>P.S. –  Some of you may ask, “Why the ‘F’ then? Why Foxymoron”. To them we say, “Why the F not?”</p>
                        <div class="view"><span class="icon-Submit-Arrow"></span></div>
                    </div>
                </li>
                <li>
                    <div class="latest-tag">
                        Latest
                        <hr>
                    </div>
                    <div class="content-text">
                        <div class="case-study-tag">
                            <div class="upperline"></div>
                            Case Study
                        </div>
                        <h3>Motorolla's <span class="text-color-primary">Biggest</span> Phone Launch Ever - <span class="text-color-primary">The Z2 Play</span></h3>
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."</p>
                        <div class="view"><span class="icon-Submit-Arrow"></span></div>
                        <div class="case-study-note">
                            Digital Marketing, Media Planning & Buying
                        </div>
                    </div>
                </li>
                <li>
                    <div class="content-text">
                        <div class="case-study-tag">
                            <div class="upperline"></div>
                            Case Study
                        </div>
                        <h3>Lorem ipsum dolor sit amet, <span class="text-color-primary">consectetur adipiscing</span> elit.</h3>
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."</p>
                        <div class="view"><span class="icon-Submit-Arrow"></span></div>
                    </div>
                </li>
                <li>
                    <div class="content-text">
                        <div class="case-study-tag">
                            <div class="upperline"></div>
                            Case Study
                        </div>
                        <h3>Lorem ipsum dolor sit amet, <span class="text-color-primary">consectetur adipiscing</span> elit.</h3>
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."</p>
                        <div class="view"><span class="icon-Submit-Arrow"></span></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div id="fullpage">
        <div class="section page1" data-bgcolor="#033a68"></div>
        <div class="section page2" data-bgcolor="#142d14"></div>
        <div class="section page3" data-bgcolor="#4f3724"></div>
        <div class="section page4" data-bgcolor="#933405"></div>
    </div>
</div>

<?php get_footer(); ?>
