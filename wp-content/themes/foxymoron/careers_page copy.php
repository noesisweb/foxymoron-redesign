<?php /* Template Name: careers_single */ ?>
<?php get_header(); ?>


<div class="container">
	<div class="careers-page">
		<div class="careers-block-category">
			<a href="">Careers <span>//</span>All</a>
		</div>
		<div class="careers-social-links"> 
			<ul>
				<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			</ul>
		</div>
		<h3>UI / UX Designer - Mumbai</h3>
		<span>Craft the next generation of Experiences and Interfaces. Designer with an Engineer's mind.</span>
		<div class="careers-page-content">
			<h4><span>Go</span>od to have</h4>
			<ul>
				<li> Experience in Front end Development.</li>
				<li> Ability to interact with clients.</li>
				<li> Dog Lover, Gotham and Bodoni work with us part time.</li>
			</ul>
			<h4><span>We</span> are looking for</h4>
			<ul>
				<li> Intelligent and Articulate. Independent Thinker. Brings fresh perspectives.</li>
				<li> T-shaped people. Experts in their area, aware of many others. Ability to cross-pollinate ideas.</li>
				<li> Skilled Craftsmen. Love their craft and getting their hands dirty. No armchair experts.</li>
				<li> Believe that everything yields to hardwork. And that nothing significant was ever achieved without grit.</li>
			</ul>
			<h4><span>Ke</span>y Responsibilities</h4>
			<ul>
				<li> Design user interfaces for digital platforms including marketing sites, web applications, eCommerce sites, 	
    mobile apps and digital products.</li>
				<li>Create sitemaps and wireframes for responsive design with a strong eye for layout and structure.</li>
				<li>Map user personas and desired actions. Be the Usability Champion in the team.</li>
				<li>Taking lead on a project; all the way from leading kickoff sessions, research exercises, and presenting work to 
    clients throughout the process.</li>
				<li>Work closely with Front End Engineer. Document features for seamless handoff to design and development 
    teams.</li>
			</ul>
			<h4>Apply For This Job</h4>
			<p>No problem. We are always on the lookout for smart, talented and passionate individuals who would like to work with us. Fill in the form below and we will keep your CV on record. If something matching your profile opens up, we will get in touch!</p>

			<div class="foxyform careers-form">
				<?php gravity_form(6, false, false, false, '', true, 12); ?>
			</div>
		</div>
	</div>
	<div class="news-wrapper">
		<div class="container">	
			<div class="case-study-wrapper">
	
	        	<div class="cb-block news-block">
					<div class="news-block-category">Careers<span>//</span>Delhi NCR</div>
					<h3><a href="#">UI / UX Designer - Delhi NCR</a></h3>
					<span class="fa fa-arrow-right arrow-right"></span>
	        	</div>
	
				<div class="cb-block news-block">
					<div class="news-block-category">Careers<span>//</span>Delhi NCR</div>
					<h3><a href="#">Jr. UI / UX Designer - Delhi NCR</a></h3>
					<span class="fa fa-arrow-right arrow-right"></span>
	        	</div>
	
				<div class="cb-block news-block">
					<div class="news-block-category">Careers<span>//</span>Delhi NCR</div>
					<h3><a href="#">Sr. UI / UX Designer - Bengaluru</a></h3>
					<span class="fa fa-arrow-right arrow-right"></span>
	        	</div>
	
				<div class="cb-block news-block">
					<div class="news-block-category">Careers<span>//</span>Delhi NCR</div>
					<h3><a href="#">UI / UX Designer - Bengaluru</a></h3>
	            	<span class="fa fa-arrow-right arrow-right"></span>
		        </div>
	        	
			</div>	
		</div>
	</div>
</div>


<?php get_footer();?>