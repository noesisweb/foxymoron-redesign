<?php get_header(); ?>

	<?php 
			$postID = get_the_ID();
			$categories = wp_get_post_categories($postID);
			foreach($categories as $c){
				$cat = get_category($c);
				if($cat->category_parent == 0){
					$parent_category = $cat;
				}
				else{
					$sub_category = $cat;
				}
			}
			$category_url= get_category_link( $parent_category->term_id );
			
			$short_description = get_field('short_description');
			if (has_post_thumbnail()) { ?>
				<div class="inner-news1">
					<div class="container featured_image">
					<?php	
							$cover_image_id = get_field('cover_image');
							if ($cover_image_id) {
								if(!wp_is_mobile()){
									$image = wp_get_attachment_image_src( $cover_image_id , 'banner_desktop' );
								} else {
									$image = wp_get_attachment_image_src( $cover_image_id , 'banner_mobile' );
								}
							
							?>
							<img  src="<?php if(!empty($image)) echo $image[0]; ?>" alt="case-studys">
							<?php
						} 
					?>
					</div>
				</div>
				<div class="inner-news1-block">
					<div class="container">
						<div class="row">
							<div class="col-md-3">	
								<div class="case-study-wrapper">
									<div class="cb-block news-block">
										
										<div class="news-block-category"><a href="<?php echo $category_url; ?>">News</a><span>//</span><?php echo $sub_category->name; ?></div>
										<h3><?php single_post_title(); ?> </h3>
										<p><?php 
											if($short_description){
												echo $short_description;
											}
											?>
										</p>
										<div class="inner-news1-icons">
											<?php echo do_shortcode('[Sassy_Social_Share ]'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-9 inner-news1-details">
								<p>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
										<?php the_content(); ?>
									<?php endwhile; endif;?>
								</p>
							</div>
						</div>	
					</div>
				</div>
			<?php
			}
			else {
			?>
				<div class="inner-news2-block">
					<div class="container">	
						<div class="news2-block">
							<div class="news-block-category"><a href="<?php echo $category_url; ?>">News</a><span>//</span><?php echo $sub_category->name; ?></div>
								<div class="inner-news2-icons"> 
									<?php echo do_shortcode('[Sassy_Social_Share ]'); ?>
								</div>
							<div style="clear:both;"></div>
							<h2><a href="#"><?php single_post_title(); ?> </a></h2>
							<p><?php 
								if($short_description){
									echo $short_description;
								}
								?>
							</p>
							<p>
								<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<?php the_content(); ?>
								<?php endwhile; endif;?>
							</p>
						</div>
					</div>
				</div>
			<?php
			}
	?>

<?php
	
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 4,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'News',
		'post__not_in' => array($postID)
	);

	$featured_posts = new WP_Query( $post_args );
?>

<div class="inner-news1-wrapper">
	<div class="container">	
		<div class="case-study-wrapper">
			<?php
				if($featured_posts->have_posts()) {
					while($featured_posts->have_posts()) {
						$featured_posts->the_post();
						$postID = get_the_ID();
						$categories = wp_get_post_categories($postID); 
						foreach($categories as $c){
							$cat = get_category($c);
							if($cat->category_parent != 0){
								$sub_category = $cat;
							}
						}
						$post_url = get_post_permalink();
						?>
							<div class="cb-block news-block">
								<div class="news-block-category"><a href="">News</a><span>//</span><?php echo $sub_category->name; ?></div>
					            <h3><a href="<?php echo $post_url; ?>"><?php the_title(); ?></a></h3>
					            <a href="<?php echo $post_url; ?>"><span class="fa fa-arrow-right arrow-right"></span></a>
					        </div>
						<?php
					}
				}
			?>
		</div>
	</div>
</div>


<?php get_footer();?>