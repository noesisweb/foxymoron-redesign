<?php
/**
 * Include Framework Modules
 *
 * The $file_includes array determines the code libraries included in your theme.
 *
 * @since 0.0.1
 */
$file_includes = array(
    'actions.php',
    'assets.php',
    'core-functions.php',
    'init.php',
    'filters.php'
);

// Include files from $csco_includes array.
foreach ($file_includes as $file) {
    include_once get_stylesheet_directory() . '/framework/' . $file;
}

require get_template_directory() . '/inc/customizer.php';
$req_files = array(
	'/inc/custom-header.php',
	'/inc/template-tags.php',
	'/inc/template-functions.php'
);

foreach($req_files  as $file) {
	require get_template_directory() . $file;	
}

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


//Disable admin bar
if ( current_user_can( 'manage_options' ) ) {
    show_admin_bar( false );
}
