<?php get_header(); ?>
<div class="page-pr-list">
	<div class="horizontal-scroll-wrapper">
		<article class="single-block no-image">
			<div class="article-content">
				<div class="content">
					<div class="article-title"><div class="upperline"></div><p>Archived</br>PR List</p></div>
					<p class="article-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p class="article-list"><a href="#">Press Release</a></p>
				</div>
			</div>
		</article>
		<?php if ( have_posts() ) : 
			while ( have_posts() ) : the_post();
		?>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<div><a href="<?php echo get_permalink() ?>"><?php echo get_field('formatted_post_title'); ?></a></div>
					</div>
				</div>
			</div>
		</article>
		<?php 
			endwhile;
			endif; 
		?>
	</div>
	<!--
<div class="horizontal-scroll-navigation">
		<button class="navigation navigation-left"><span class="fa fa-arrow-left"></span></button>
		<button class="navigation navigation-right"><span class="fa fa-arrow-right"></span></button>
	</div>
-->
</div>
<?php get_footer(); ?>