<?php
/*
	Template Name: Contact Us Old
*/
get_header();
?>

<div class="contact-us-block" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/contactsbg.jpg)">
    <div class="contact-us-bg">
        <div class="container">
            <div class="contact-us">
                <h3>Get In Touch & Let’s See What We Can Do Together</h3>
                <div class="contact-us-inquiries">
                    <div class="business-inquiries">
                        <span>New Business Inquiries</span>
                        <span class="new-business-inquiries"><a href="mailto:bd@foxymoron.in">bd@foxymoron.in</a></span>
                    </div>
                    <div class="business-inquiries">
                        <span>Work At FoxyMoron</span>
                        <span><a href="<?php echo site_url(); ?>/careers">Visit our Careers section</a></span>
                    </div>
                    <div class="business-inquiries">
                        <span>Press Inquiries</span>
                        <span><a href="mailto:mahtab@foxymoron.org">mahtab@foxymoron.org</a></span>
                    </div>
                    <div class="business-inquiries">
                        <span>Everything Else</span>
                        <span><a href="mailto:info@foxymoron.in">info@foxymoron.in</a></span>
                    </div>
                </div>
            </div>	 			
        </div>
    </div> 
</div>
<?php
$post_args = array(
    'post_type' => 'contact_us',
    'post_status' => 'publish',
    'order_by' => 'date',
    'order' => 'ASC',
);
$contact_us_posts = new WP_Query($post_args);
?>

<div class="contacts-us-details">
    <div class="container">
        <div class="contacts-us-content">
            <?php
            if ($contact_us_posts->have_posts()) {
                while ($contact_us_posts->have_posts()) {
                    $contact_us_posts->the_post();

                    if (has_post_thumbnail(get_the_ID())) {
                        $image = wp_get_attachment_image_src(get_post_thumbnail_id($contact_us_posts->ID), 'contactus_img');
                    } else {
                        $image = '';
                    }
                    ?>
                    <div class="contact-row clearfix">
                        <div class="col-md-4 contact-img-refrence">
                            <div class="contacts-mumbai">
                                <div class="contact-img"><img src="<?php if (!empty($image)) echo $image[0]; ?>" alt="case-study"></div>
                                <span><?php the_title(); ?></span>
                            </div>
                        </div>
                        <div class="col-md-8 contact-reference contact-reference1">
                            <div class="contact">
                                <h4>Drop By</h4>
                                <span><?php echo get_field('drop_by'); ?></span>
                            </div>
                            <div class="contact">
                                <h4>Give Us A Call</h4>
                                <span><a href="tel:<?php echo get_field('give_us_a_call'); ?>"><?php echo get_field('give_us_a_call'); ?></a></span>
                            </div>
                            <div class="contact">
                                <h4>Want To Join The Team?</h4>
                                <span><?php echo get_field('join_the_team'); ?></span>
                            </div>
                            <div class="contact">
                                <h4>In <?php the_title(); ?>, We Partner With</h4>
                                <div class="contact-patnership">
                                    <?php
                                    $partner_images = get_field('partner_with');
                                    if (!empty($partner_images)) {
                                        foreach ($partner_images as $partner) {
                                            $image = wp_get_attachment_image($partner['partner_logo'], 'contact_us_partner');
                                            ?>
                                            <div class="contact-patners"><?php echo $image; ?></div>
                                            <?php
                                            }
                                        }
                                        ?>	
                                </div>
                            </div>						
                        </div>
                    </div><!-- end section -->
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<?php
get_footer();
