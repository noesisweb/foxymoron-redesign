<?php /* Template Name: careers Landing Page */ ?>
<?php get_header(); ?>

<div class="page-career">
  	<div class="section">
  		<div class="content">
  			<div class="content-text active-block">
	          <div class="heading">
	            <span class="text-color-primary">
	              <div class="upperline"></div>
	              Careers</span>
	          </div>
	          <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
	
	          <div class="career-devisions">
	            <div class="foxy-radio">
	              <input type="radio" name="careerDevisions" id="city" value="city">
	              <label for="city" class="radio-label">By City
	                <div class="count">30 positions</div>
	              </label>
	            </div>
	            <div class="foxy-radio">
	              <input type="radio" name="careerDevisionsopening" id="openings" value="openings">
	              <label for="openings" class="radio-label">By Openings
	                <div class="count">30 positions</div>
	              </label>
	            </div>
	          </div>
        </div>

        <div class="content-text">
	      <button  class="back-button" onclick="changeCareerSlide(1)"><i class="fa fa-chevron-left"></i> Back</button>
          <div class="heading">
            <span class="text-color-primary">
              <div class="upperline"></div>
              By City</span>
          </div>
          <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>
          <div class="career-devisions careerbyDivision">
            <div class="foxy-radio">
              <input type="radio" name="careerbyCity" id="allcity" value="allcity">
              <label for="allcity" class="radio-label">All
                <div class="count">30 openings</div>
              </label>
              <ul class="career-designations">
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
                <li>Content Manager</li>
              </ul>
            </div>
            <div class="foxy-radio">
              <input type="radio" name="careerbyCity" id="mumbai" value="mumbai">
              <label for="mumbai" class="radio-label">Mumbai
                <div class="count">30 openings</div>
              </label>
              <ul class="career-designations">
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
              </ul>
            </div>
            <div class="foxy-radio">
              <input type="radio" name="careerbyCity" id="delhincr" value="delhincr">
              <label for="delhincr" class="radio-label">Delhi NCR
                <div class="count">10 openings</div>
              </label>
              <ul class="career-designations">
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
              </ul>
            </div>
            <div class="foxy-radio">
              <input type="radio" name="careerbyCity" id="bengaluru" value="bengaluru">
              <label for="bengaluru" class="radio-label">Bengaluru
                <div class="count">10 openings</div>
              </label>
              <ul class="career-designations">
                <li>Junior Content Executive</li>
                <li>Content Manager</li>
              </ul>
            </div>
          </div>
        </div>

        <div class="content-text">
	      <button class="back-button" onclick="changeCareerSlide(2)"><i class="fa fa-chevron-left"></i> Back</button>
          <div class="heading">
            <span class="text-color-primary">
              <div class="upperline"></div>
              Junior Content Executive
            </span>
          </div>
          <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Aenean sollicitudin, lorem quis bibendum auctor,  nisi elit consequat ipsum</p>

          <p>
             Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.
          </p>
          
          <div class='form-wrapper'>
	          <?php echo do_shortcode('[gravityform id=8 title=false description=false ajax=true tabindex=49]'); ?>
          </div>
        </div>
  		</div>
  </div>
</div>

<?php get_footer(); ?>
