<?php
get_header();
$city_names = get_terms('cities');
$allopening = get_posts(
        array(
            'posts_per_page' => -1,
            'post_type' => 'career',
        )
);
?>

<div class="page-career" style="background-image: url(<?php the_field('career_background', 'option'); ?>);">
    <div class="section">
        <div class="content">
            <div class="content-text active-block">
                <div class="heading">
                    <span class="text-color-primary"><span class="upperline"></span>Careers</span>
                </div>
<!--                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>-->
                <div class="career-devisions">
                    <div class="foxy-radio">
                        <input type="radio" name="careerDevisions" id="city" value="city">
                        <label for="city" class="radio-label">By City
                            <div class="count"><?php echo count($allopening); ?> positions</div>
                        </label>
                    </div>
                    <div class="foxy-radio">
                        <input type="radio" name="careerDevisions" id="openings" value="openings">
                        <label for="openings" class="radio-label">By Openings
                            <div class="count"><?php echo count($allopening); ?> positions</div>
                        </label>
                    </div>
                </div>
            </div>

            <div class="content-text">
                <button  class="back-button" onclick="changeCareerSlide(1)"><i class="fa fa-chevron-left"></i></button>
                <div class="heading">
                    <span class="text-color-primary"><span class="upperline"></span>By City</span>
                </div>
<!--                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>-->
                <div class="career-devisions careerbyDivision">
                    <div class="foxy-radio">
                        <input type="radio" name="careerbyCity" id="allcity" value="allcity">
                        <label for="allcity" class="radio-label">All
                            <div class="count"><?php echo count($allopening); ?> openings</div>
                        </label>
                        <ul class="career-designations">
                            <?php foreach ($allopening as $singleopening) { ?>
                                <li data-career_id="<?php echo $singleopening->ID; ?>" data-career_title="<?php echo $singleopening->post_title; ?>" data-city="All"><?php echo $singleopening->post_title; ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php foreach ($city_names as $city) { ?>
                        <div class="foxy-radio">
                            <input type="radio" name="careerbyCity" id="<?php echo $city->slug; ?>" value="<?php echo $city->slug; ?>">
                            <label for="<?php echo $city->slug; ?>" class="radio-label"><?php echo $city->name; ?>
                                <div class="count"><?php echo $city->count; ?> openings</div>
                            </label>
                            <?php
                            $cityposts = get_posts(
                                    array(
                                        'posts_per_page' => -1,
                                        'post_type' => 'career',
                                        'tax_query' => array(
                                            array(
                                                'taxonomy' => 'cities',
                                                'field' => 'term_id',
                                                'terms' => $city->term_id,
                                            )
                                        )
                                    )
                            );
                            ?>
                            <ul class="career-designations">
                                <?php foreach ($cityposts as $singlecitypost) { ?>
                                    <li data-career_id="<?php echo $singlecitypost->ID; ?>" data-career_title="<?php echo $singlecitypost->post_title; ?>" data-city="<?php echo $city->name; ?>"><?php echo $singlecitypost->post_title ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="content-text">
                <button  class="back-button" onclick="changeCareerSlide(1)"><i class="fa fa-chevron-left"></i></button>
                <div class="heading">
                    <span class="text-color-primary"><span class="upperline"></span>By Openings</span>
                </div>
<!--                <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum.</p>-->
                <div class="career-devisions careerbyDivision">
                    <?php foreach ($allopening as $singleopening) { ?>
                        <div class="foxy-radio">
                            <input type="radio" name="careerbyopening" id="<?php echo $singleopening->ID; ?>" value="<?php echo $singleopening->ID; ?> ?>">
                            <label for="<?php echo $singleopening->ID; ?>" class="radio-label"><?php echo $singleopening->post_title; ?></label>
                            <?php $postcities = wp_get_post_terms($singleopening->ID, 'cities'); ?>
                            <ul class="career-designations">
                                <?php foreach ($postcities as $postcity) { ?>
                                    <li data-career_id="<?php echo $singleopening->ID; ?>" data-career_title="<?php echo $singleopening->post_title; ?>" data-city="<?php echo $postcity->name; ?>"><?php echo $postcity->name; ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="content-text career_details_section">
                <button class="back-button" onclick="changeCareerSlide(2)"><i class="fa fa-chevron-left"></i> Back</button>
                <div class="heading">
                    <span class="text-color-primary">
                        <div class="upperline"></div>
                        <div class="career_title"></div>
                    </span>
                </div>
                <div class="career_desc"></div>
                <div class='form-wrapper'>
                    <?php echo do_shortcode('[gravityform id=8 title=false description=false ajax=true tabindex=49]'); ?>
                </div>
            </div>
            <?php
            if (!empty($allopening)) {
                foreach ($allopening as $opening) {
                    ?>
                    <div id="career_desc_<?php echo $opening->ID; ?>" style="display: none;"><?php echo $opening->post_content; ?></div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<?php
get_footer();
