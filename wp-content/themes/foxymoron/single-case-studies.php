<?php get_header(); ?>

<div class="foxy-banner case-study-banner1">
	<div class="container">
		<?php
			$cover_image_id = get_field('cover_image');
			if ($cover_image_id) {
				if(!wp_is_mobile()){
					$image = wp_get_attachment_image_src( $cover_image_id , 'banner_desktop' );
				} else {
					$image = wp_get_attachment_image_src( $cover_image_id , 'banner_mobile' );
				}
				
				?>
				<img src="<?php if(!empty($image)) echo $image[0]; ?>" alt="case-studys">
				<?php
			} 
		?>
	</div>
</div>
<div class="container ">
	<div class="case-study-description">
		<div class="case-study-text">
	
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		<?php the_content(); ?>
		
		<?php endwhile; endif;?>

		</div>
	</div>
</div>

<?php
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 8,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'Case Studies',
	);

	$featured_posts = new WP_Query( $post_args );
?>
<div class="case-study-work">
	<div class="container">	
		<h2>Some Other Cool Work</h2>
		<div class="case-study-wrapper">
			<?php
			if($featured_posts->have_posts()) {
				while($featured_posts->have_posts()) {
					
				$featured_posts->the_post();
				if (has_post_thumbnail( get_the_ID()) ) {
					if(!wp_is_mobile()){
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'cool_work_desktop' );
					} else {
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'common_posts_mobile' );
					}
					
				} else {
					$image = '';
				}
				?>
				<a href="<?php echo get_permalink(get_the_ID()); ?>">
			        <div class="cb-block case-study-block">
			            <img src="<?php if(!empty($image)) echo $image[0]; else echo get_template_directory_uri().'/assets/images/df-foxy.jpg'; ?>" alt="case-studys">
			            <div class="cs-info info">
			                <h3><?php the_title(); ?></h3>
			                <p><?php echo get_field('short_description'); ?></p>
			                <span class="fa fa-arrow-right arrow-right"></span>
			            </div>
			        </div>
		        </a>
		        <?php
		        }
		    }
		    wp_reset_query();
	        ?>
		</div>	
	</div>
</div>


<?php get_footer();?>
