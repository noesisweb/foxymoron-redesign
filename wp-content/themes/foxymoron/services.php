<?php /* Template Name: Services */ ?>
<?php get_header(); ?>
<div class="page-services" id="services">
	<div class="swiper-container services-swiper-container">
        <div class="swiper-wrapper">
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services1.png" /></div>
				<div class="content">
					<div class="number text-color-primary">01</div>
					<div class="heading"><span class="text-color-tertiary">Strategy <br></span><span class="text-color-primary">  & Management</span></div>
					<p>Digital Strategy Development, Account Management</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services2.png" /></div>
				<div class="content">
					<div class="number text-color-primary">02</div>
					<div class="heading"><span class="text-color-tertiary">Content</span><br><span class="text-color-primary">Engine</span></div>
					<p>Content Strategy Development, Social Media Content development, Long Format Content Creation, Technical Content Creation, Search & Media Content Creation</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services3.png" /></div>
				<div class="content">
					<div class="number text-color-primary">03</div>
					<div class="heading"><span class="text-color-tertiary">Content<br></span><span class="text-color-primary">Production</span></div>
					<p>Video Planning & Stratregy Services, Production Services, Post Production Services</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services4.png" /></div>
				<div class="content">
					<div class="number text-color-primary">04</div>
					<div class="heading"><span class="text-color-tertiary">Content<br></span><span class="text-color-primary">Partnerships</span></div>
					<p>Influencer Strategy & Management, Content Creators Strategy & Management, Sponsorhips, Brand Integrations </p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services5.png" /></div>
				<div class="content">
					<div class="number text-color-primary">05</div>
					<div class="heading"><span class="text-color-tertiary">Media<br></span><span class="text-color-primary"> Services</span></div>
					<p>Programmatic Services, Reach & Frequency Planning & Buying, Performance Marketing, Paid Search Marketing, Native Planning & Buying</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services6.png" /></div>
				<div class="content">
					<div class="number text-color-primary">06</div>
					<div class="heading"><span class="text-color-tertiary">Search<br></span><span class="text-color-primary"> Services</span></div>
					<p>Search Strategy (Paid+ Unpaid), Search Engine Optimization, Paid Search Marketing</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services7.png" /></div>
				<div class="content">
					<div class="number text-color-primary">07</div>
					<div class="heading"><span class="text-color-tertiary">Technology<br></span><span class="text-color-primary"> Services</span></div>
					<p>Web Asset Strategy Planning, User Jounrey and experience design, Product Interface Design, Product Development and build, Testing (product + user), Web & App Maintenance, Creative Techology</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
				
			</div>
			
			<div class="swiper-slide section">
				<div class="content-image"><img src="<?php echo get_template_directory_uri();?>/assets/img/abt-services8.png" /></div>
				<div class="content">
					<div class="number text-color-primary">08</div>
					<div class="heading"><span class="text-color-tertiary">Listening <br></span><span class="text-color-primary"> & ORM</span></div>
					<p>Brand Conversation Listening, Competition Listening, Analysis of Listening Data, Response Management (ORM)</p>
<!-- 					<i class="fa fa-arrow-right"></i> -->
				</div>
			</div>
			
			
        </div>
    </div>
</div>

<?php get_footer(); ?>