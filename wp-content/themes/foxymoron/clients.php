<?php
/* Template Name: Clients */
get_header();

$client_details = get_field('client_details');
if (!empty($client_details)) {
    ?>
    <div class="page-clients dragscroll">
        <div class="drag-content">
            <div class='foxy-grid-wrapper'>
                <?php
                foreach ($client_details as $client) {
                    $client_logo_id = $client['client_logo'];
                    $client_logo = wp_get_attachment_image($client_logo_id, 'full');
                    ?>
                    <div class="foxy-grid-item">
                        <div class="image">
                            <?php echo $client_logo; ?>
                        </div>
                        <div class="name"><?php echo $client['client_name']; ?></div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
get_footer();
