<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foxymoron
 */
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer">

<?php if(is_singular('work') || is_singular('news')) {?>
<?php
	$fb_share_url = 'https://www.facebook.com/sharer.php?u=' . get_the_permalink(get_the_ID());
	$tw_share_url = 'http://twitter.com/share?url=' . get_the_permalink(get_the_ID()).'&text='.get_the_title();
	//$pin_share_url = 'http://pinterest.com/pin/create/button/?url=' . get_the_permalink(get_the_ID());
	$g_plus_share_url = 'https://plus.google.com/share?url=' . get_the_permalink(get_the_ID());
?>
 <div class="social-icons">
    <ul>
    <li><a href="<?php echo $fb_share_url; ?>" target="_blank"><span class="icon-Grey-Facebook icon"></span></a></li>
    <li><a href="<?php echo $tw_share_url; ?>" target="_blank"><span class="icon-Grey-Twitter icon"></span></a></li>
    <li><a href="<?php echo get_field('dribble_link', 'option'); ?>" target="_blank"><span class="icon-Grey-Dribbble icon"></span></a></li>
    <li><a href="<?php echo $g_plus_share_url; ?>" target="_blank"><span class="icon-Grey-Google-Plus icon"></span></a></li>
    </ul>
</div>
<?php } else { ?>

	<div class="social-icons">
        <ul>
        <li><a href="<?php echo get_field('facebook_link', 'option'); ?>" target="_blank"><span class="icon-Grey-Facebook icon"></span></a></li>
        <li><a href="<?php echo get_field('twitter_link', 'option'); ?>" target="_blank"><span class="icon-Grey-Twitter icon"></span></a></li>
        <li><a href="https://www.instagram.com/beingfoxy/" target="_blank" ><span class="iconn-instapage icon2"></span></a></li>
        <li><a href="https://www.linkedin.com/company/foxymoron/" target="_blank"><span class="iconn-linkedin icon2"></span></a></li>
        <li><a href="https://www.youtube.com/Foxymorontv" target="_blank"><span class="iconn-youtube icon2"></span></a></li>
        
<!--         <li><a href="<?php echo get_field('dribble_link', 'option'); ?>" target="_blank"><span class="icon-Grey-Dribbble icon"></span></a></li> -->
<!--     	<li><a href="<?php echo get_field('google_link', 'option'); ?>" target="_blank"><span class="icon-Grey-Google-Plus icon"></span></a></li> -->
        </ul>
    </div>

<?php } ?>
	
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
