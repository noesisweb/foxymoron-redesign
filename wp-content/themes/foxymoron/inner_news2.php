<?php /* Template Name: inner-news2 Page */ ?>
<?php get_header(); ?>

<div class="inner-news2-block">
	<div class="container">	
			
			<div class="news2-block">
				<div class="news-block-category">News<span>//</span>New Business</div>
				<div class="inner-news2-icons"> 
					        <span><i class="fa fa-facebook-official" aria-hidden="true"></i></span>
				            <span><i class="fa fa-linkedin-square" aria-hidden="true"></i></span>
				            <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
				            <span><i class="fa fa-whatsapp" aria-hidden="true"></i></span>
			    </div>
			    <div style="clear:both;"></div>
	            <h2><a href="#">Foxy Wins The Kraft Heinz Group's Digital Business</a></h2>
	            <p>The brands we will be working on are Heinz, Kraft, Nycil, Complan and Glucon D.<span>We cannot wait to get started!</span></p>
				<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
	           <div class="inner-news2-img"><img src="<?php echo get_template_directory_uri() ?>/assets/images/inner-news2.jpg" alt="case-studys"></div>
	           <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. 
	           </p>
	        </div>
	        
	</div>
</div>

<div class="inner-news2-wrapper">
	<div class="container">	
		<div class="case-study-wrapper">
	
	        <div class="cb-block news-block">
				<div class="news-block-category">News<span>//</span>New Business</div>
	            <h3><a href="#">Foxy Wins The Kraft Heinz Group’s Digital Business</a></h3>
	            <span class="fa fa-arrow-right arrow-right"></span>
	        </div>
	
	        <div class="cb-block news-block">
				<div class="news-block-category">News<span>//</span>New Business</div>
	            <h3><a href="#">Foxy is going to be flying high after signing on GoAir</a></h3>
	            <span class="fa fa-arrow-right arrow-right"></span>
	        </div>
	
	       <div class="cb-block news-block">
				<div class="news-block-category">News<span>//</span>New Business</div>
	            <h3><a href="#">Foxy Wins The Kraft Heinz Group’s Digital Business</a></h3>
	            <span class="fa fa-arrow-right arrow-right"></span>
	        </div>
	
	        <div class="cb-block news-block">
				<div class="news-block-category">News<span>//</span>New Business</div>
	            <h3><a href="#">Foxy is going to be flying high after signing on GoAir</a></h3>
	            <span class="fa fa-arrow-right arrow-right"></span>
	        </div>
		
		</div>
	</div>
</div>





<?php get_footer();?>