<?php get_header(); ?>

<div class="careers-banner" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/careersbg.jpg)">
	<h3>We’re Always On The Lookout For People Better Than Us</h3>
</div>

<div class="container">
	<div class="careers-block">
		
		<div class="career-heading button-group clearfix">
			<ul id="filters">
				<li class="button is-checked" data-filter="*" ><a>All</a></li>
				<li class="button" data-filter=".mumbai">Mumbai</li>
				<li class="button" data-filter=".delhi">Delhi NCR</li>
				<li class="button" data-filter=".bengaluru">Bengaluru</li>
			</ul>
		</div>
		
		<div class="heading-wrapper clearfix">
			<div id="filter-wrapper" class="grid">	
				
				<div class="heading-content element-item bengaluru mumbai">
					<div class="grid-item-content">
						<span class="job-title">Media</span>
						<span>3 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>	
							</ul>
						</div>
					</div>
				</div>
				
				<div class="heading-content element-item bengaluru mumbai">
					<div class="grid-item-content">
						<span class="job-title">Design</span>
						<span>3 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="heading-content element-item bengaluru mumbai">
					<div class="grid-item-content">
						<span class="job-title">Content</span>
						<span>8 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Creative Director</li>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Creative Director</li>	
							</ul>
						</div>
					</div>				
				</div>
		
				<div class="heading-content element-item delhi mumbai">
					<div class="grid-item-content">
						<span class="job-title">Account Management</span>
						<span class="account-management">12 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Creative Director</li>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Creative Director</li>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Creative Director</li>	
							</ul>
						</div>
					</div>
				</div>
							
				<div class="heading-content element-item delhi">
					<div class="grid-item-content">
						<span class="job-title">ORM</span>
						<span>3 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="heading-content element-item mumbai">
					<div class="grid-item-content">
						<span class="job-title">UI/UX</span>
						<span>3 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="heading-content element-item delhi mumbai">
					<div class="grid-item-content">
						<span class="job-title">Technology</span>
						<span>8 Openings</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
								<li>Content Manager</li>
								<li>Junior Content Executive</li>
								<li>Senior Content Executive</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="heading-content element-item alkali mumbai">
					<div class="grid-item-content">
						<span class="job-title">Production</span>
						<span>1 Opening</span>
						<div class="heading-list">
							<ul>
								<li>Junior Content Executive</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<div class="career-content">
			<span>Don’t See An Opening You Fit?</span>
			<p>No problem. We are always on the lookout for smart, talented and passionate individuals who would like to work with us. Fill in the form below and we will keep your CV on record. If something matching your 				profile opens up, we will get in touch!</p>
		</div>
		
		<div class="foxyform">
			<?php gravity_form(1, false, false, false, '', true, 12); ?>
		</div>
	</div>
</div>





<?php get_footer();?>