<?php /* Template Name: careers_single */ ?>
<?php get_header(); ?>
<?php
/*
	$term = wp_get_post_terms( get_the_ID(), 'cities' );
	echo '<pre>';
	print_r($term);
	echo '<pre>';
	exit;
*/
?>
<?php
	if(!empty($_GET['career_city'])) {
		$career_city = $_GET['career_city'];	
	} else {
		$career_city = 'all';
	}
	
	
?>


<div class="container">
	<div class="careers-page">
		<div class="careers-block-category">
			<a href="">Careers <span>//</span><?php echo $career_city; ?></a>
		</div>
		<div class="careers-social-links"> 
			<?php echo do_shortcode('[Sassy_Social_Share ]'); ?>
<!--
			<ul>
				<li><a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
				<li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			</ul>
-->
		</div>
		
		<h3><?php the_title();?></h3>
		<span><?php the_excerpt();?></span>
		
		<div class="careers-page-content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif;?>
<!--
			<h4><span>Go</span>od to have</h4>
			<ul>
				<li> Experience in Front end Development.</li>
				<li> Ability to interact with clients.</li>
				<li> Dog Lover, Gotham and Bodoni work with us part time.</li>
			</ul>
			<h4><span>We</span> are looking for</h4>
			<ul>
				<li> Intelligent and Articulate. Independent Thinker. Brings fresh perspectives.</li>
				<li> T-shaped people. Experts in their area, aware of many others. Ability to cross-pollinate ideas.</li>
				<li> Skilled Craftsmen. Love their craft and getting their hands dirty. No armchair experts.</li>
				<li> Believe that everything yields to hardwork. And that nothing significant was ever achieved without grit.</li>
			</ul>
			<h4><span>Ke</span>y Responsibilities</h4>
			<ul>
				<li> Design user interfaces for digital platforms including marketing sites, web applications, eCommerce sites, 	
    mobile apps and digital products.</li>
				<li>Create sitemaps and wireframes for responsive design with a strong eye for layout and structure.</li>
				<li>Map user personas and desired actions. Be the Usability Champion in the team.</li>
				<li>Taking lead on a project; all the way from leading kickoff sessions, research exercises, and presenting work to 
    clients throughout the process.</li>
				<li>Work closely with Front End Engineer. Document features for seamless handoff to design and development 
    teams.</li>
			</ul>
			<h4>Apply For This Job</h4>
			<p>No problem. We are always on the lookout for smart, talented and passionate individuals who would like to work with us. Fill in the form below and we will keep your CV on record. If something matching your profile opens up, we will get in touch!</p>
-->
			
			
			

			<div class="foxyform careers-form">
				<?php gravity_form(6, false, false, false, '', true, 12); ?>
			</div>
		</div>
	</div>
	
	
	<?php
		$city_array = array();
		$city_array = wp_get_post_terms(get_the_ID(),'cities');	
	?>
	<div class="news-wrapper">
		<div class="container">	
			<div class="case-study-wrapper">
				<?php
					$count = 0;
					foreach($city_array as $city) {
						
						if($city->slug == $career_city){
							 continue;
						}
						$count++;
				?>
	        	<div class="cb-block news-block">
					<div class="news-block-category">Careers<span>//</span><?php echo $city->name;?></div>
					<h3><?php echo '<a href='.get_post_permalink(get_the_ID()).'?career_city='.$city->slug.'>'.get_the_title().' - '.$city->name.'</a>';
?></h3>			
					<?php echo '<a href='.get_post_permalink(get_the_ID()).'?career_city='.$city->slug.'>'; ?>
					<span class="fa fa-arrow-right arrow-right"></span>
					<?php echo '</a>'?>
	        	</div>
	        	<?php
		        	}
	        	?>
				<?php 
					$dept_array = array();
					$dept_array = wp_get_post_terms(get_the_ID(),'departments');
					$no_of_posts = (4 - $count); 
					$res2 = get_all_data($dept_array[0]->slug,$no_of_posts,get_the_ID());
					if($no_of_posts !=0 ) {
					foreach($res2 as $single_res) {
				?>
				<div class="cb-block news-block">
					<div class="news-block-category">Careers</div>
					<h3><a href="<?php echo get_post_permalink($single_res->ID); ?>?career_city=all"><?php echo $single_res->post_title;?></a></h3>
					<a href="<?php echo get_post_permalink($single_res->ID)?>.?career_city=all">
						<span class="fa fa-arrow-right arrow-right"></span>
					</a>
	        	</div>
	        	<?php } }?>	        	
			</div>	
		</div>
	</div>
</div>


<?php get_footer();?>