<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package foxymoron
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1397317747169216';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<div id="page" class="site">
	<div id="page-overlay">
		<div class="lds-dual-ring"></div>
	</div>

<!-- 	<div id="content" class="site-content"> -->
<nav class="navbar navbar-default">
	<div class="top-menu">
		<div class="col-xs-12">
			<div class="menu-container">
				<div class="logo-wrapper">
					<a href="<?php echo get_field('phosphene_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/phosphene.png"/></div></a>
					<a href="<?php echo get_field('rabbit_hole_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/rabbithole.png"/></div></a>
					<a href="<?php echo get_field('pollen_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/Pollen.png"/></div></a>
					<a href="<?php echo get_field('zoomedia_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/zoomedia.png"/></div></a>
				</div>
				<div class="menu-items-wrapper">
					<div class="menu-items">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'Primary-menu',
								'menu_class' => 'menu-items', 
							) );
						?>
						
						<ul class="logo-item">
							<li><a href="<?php echo get_field('phosphene_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/phosphene.png"/></div></a></li>
							<li><a href="<?php echo get_field('rabbit_hole_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/rabbithole.png"/></div></a></li>
							<li><a href="<?php echo get_field('pollen_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/Pollen.png"/></div></a></li>
							<li><a href="<?php echo get_field('zoomedia_url', 'option'); ?>" target="_blank"><div class="logoimg"><img src="<?php echo get_template_directory_uri() ?>/assets/img/zoomedia.png"/></div></a></li>
						</ul>
						
						
						
					</div>
					<div class="menu-news">
						<?php 
							$news = get_posts(
							    array(
							        'posts_per_page' => 2,
							        'post_type' => 'news',
							        'tax_query' => array(
							            array(
							                'taxonomy' => 'news-category',
							                'field' => 'slug',
							                'terms' => 'news_cat',
							            )
							        )
							    )
							);
							//print_r($news);
						?>
						<?php foreach($news as $singlenews ){?>
						<div class="single-menu-post">
							<h3 class="post-heading"><div class="upperline"></div><?php echo $singlenews -> post_title;?></h3>
							<p class="describtion"><?php echo $singlenews -> post_content;?></p>
<!--
							<?php 
								echo '<pre>';
									print_r($singlenews);
								echo '</pre>';
							?>
-->
							<a class="arrow" href="<?php site_url(); ?>/news/<?php echo $singlenews -> post_name;?>"><span class="icon-Submit-Arrow"></span></a>
						</div>
						<?php } ?>
					</div>
				</div>	
				<div class="menu-bottom">
					<p class="email"><a href="mailto:info@foxymoron.in">info@foxymoron.in</a></p>
					<p class="copyright">© Copyright 2018</p>
				</div>
			</div>
			<div class="col-md-2 header-wrapper"> 	
				<?php
					if( !is_singular( 'news' ) ) {   
				?>
				<div class="menu">
					<span class="menu1"></span>
					<span class="menu2"></span>
					<span class=""></span>
				</div>
		
				<div class="foxy-logo">
					<a href="<?php echo site_url();?>">
						<img class="light" src="<?php echo get_template_directory_uri();?>/assets/img/logowhite.png" width="150px"/>
						<img class="dark" src="<?php echo get_template_directory_uri();?>/assets/img/logoblack.png" width="150px"/>
 					</a>
				</div>
				<?php
					}
					else{
				?>
				<a href="<?php echo site_url();?>/news" class="back-to-news"> 
					<i class="fa fa-times news-back-button"></i>
					<div class="upperline"></div>
				</a>
				<?php } ?>	
				</div>
			</div>
	</div>
</nav>

<!-- <div style="clear:both;"></div> -->



			
			
			
			