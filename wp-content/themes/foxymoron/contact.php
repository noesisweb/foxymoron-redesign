<?php 
	/* Template Name: contact */ 
 	get_header();
?>

<div class="page-contact">
	<div class="horizontal-scroll-wrapper">
		<div class="single-block-wrapper">
		<div class="single-block">
			<div class="overlay"></div>
			<video id="bgvid" playsinline muted loop>
				<source src="<?php echo get_template_directory_uri() ?>/assets/video/dummy1.mp4?>" type="video/mp4">
			</video>
			<div class="article-content">
				<div class="heading"><div class="upperline"></div><p><span>Mumbai</span></p></div>
				<p class="description">2nd Floor,Amir Industrial Estate,Sun Mill Compound,Lower Parel,Mumbai,Maharashtra - 400013 </p>
				<p class="description">+91 22 6606 5555 </p>
			</div>
			
		</div>
		</div>
		<div class="single-block-wrapper">
		<div class="single-block">
			<div class="overlay"></div>
			<video id="bgvid" playsinline muted loop>
				<source src="<?php echo get_template_directory_uri() ?>/assets/video/dummy2.mp4?>" type="video/mp4">
			</video>
			<div class="article-content">
				<div class="heading"><div class="upperline"></div><p><span>Delhi NCR</span></p></div>
				<p class="description">2nd Floor,Amir Industrial Estate,Sun Mill Compound,Lower Parel,Mumbai,Maharashtra - 400013 </p>
				<p class="description">+91 22 6606 5555 </p>
			</div>
		</div>
		</div>
		<div class="single-block-wrapper">

		<div class="single-block">
			<div class="overlay"></div>
			<video id="bgvid" playsinline muted loop>
				<source src="<?php echo get_template_directory_uri() ?>/assets/video/dummy1.mp4?>" type="video/mp4">
			</video>
			<div class="article-content">
				<div class="heading"><div class="upperline"></div><p><span>Bengaluru</span></p></div>
				<p class="description">2nd Floor,Amir Industrial Estate,Sun Mill Compound,Lower Parel,Mumbai,Maharashtra - 400013 </p>
				<p class="description">+91 22 6606 5555 </p>
			</div>
		</div>
		</div>
	</div>
	<div class="contact-footer">
		<div class="flex-wrapper">
			<span class="title">New Business Inquires : </span>
			<span class="value"> bd@foxymoron.in</span>
		</div>
		<div class="flex-wrapper">
			<span class="title">Press Inquires : </span>
			<span class="value"> mahtab@foxymoron.org</span>
		</div>
		<div class="flex-wrapper">
			<span class="title">Everything Else : </span>
			<span class="value">info@foxymoron.in</span>
		</div>
	</div>
</div>
<?php
	get_footer();
?>