<?php 
 	get_header();
?>

<div class="page-contact">
	<div class="horizontal-scroll-wrapper">
		<?php while (have_posts()) : the_post();?>
			<div class="single-block-wrapper">
				<div class="single-block">
					<div class="overlay"></div>
					<video id="bgvid" playsinline muted loop>
						<source src="<?php echo get_field('video'); ?>" type="video/mp4">
					</video>
					<div class="article-content">
						<div class="heading"><div class="upperline"></div><p><span><?php echo the_title();?></span></p></div>
						<div class="description"><?php echo the_content();?></div>
					</div>
			
				</div>
			</div>
		<?php endwhile; ?>
<!--
		<div class="single-block-wrapper">
		<div class="single-block">
			<div class="overlay"></div>
			<video id="bgvid" playsinline muted loop>
				<source src="<?php echo get_template_directory_uri() ?>/assets/video/dummy2.mp4?>" type="video/mp4">
			</video>
			<div class="article-content">
				<div class="heading"><div class="upperline"></div><p><span>Delhi NCR</span></p></div>
				<p class="description">2nd Floor,Amir Industrial Estate,Sun Mill Compound,Lower Parel,Mumbai,Maharashtra - 400013 </p>
				<p class="description">+91 22 6606 5555 </p>
			</div>
		</div>
		</div>
		<div class="single-block-wrapper">

		<div class="single-block">
			<div class="overlay"></div>
			<video id="bgvid" playsinline muted loop>
				<source src="<?php echo get_template_directory_uri() ?>/assets/video/dummy1.mp4?>" type="video/mp4">
			</video>
			<div class="article-content">
				<div class="heading"><div class="upperline"></div><p><span>Bengaluru</span></p></div>
				<p class="description">2nd Floor,Amir Industrial Estate,Sun Mill Compound,Lower Parel,Mumbai,Maharashtra - 400013 </p>
				<p class="description">+91 22 6606 5555 </p>
			</div>
		</div>
		</div>
-->
	</div>
	<div class="contact-footer">
		<div class="flex-wrapper">
			<span class="title">New Business Inquires : </span>
			<span class="value"> <a href="mailto:bd@foxymoron.in">bd@foxymoron.in</a></span>
		</div>
		<div class="flex-wrapper">
			<span class="title">Press Inquires : </span>
			<span class="value"> <a href="mailto:mahtab@foxymoron.in">mahtab@foxymoron.in</a></span>
		</div>
		<div class="flex-wrapper">
			<span class="title">Everything Else : </span>
			<span class="value"><a href="mailto:info@foxymoron.in">info@foxymoron.in</a></span>
		</div>
		
		<div class="flex-wrapper">
			<span class="title">Join Us : </span>
			<span class="value"><a href="mailto:careers@foxymoron.in">careers@foxymoron.in</a></span>
		</div>
		
		
	</div>
</div>

<?php
	get_footer();
?>