<?php get_header();
	global $post;
?>

<?php

	function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
	
?>


<div class="page-work-inside">
    <div class="section container-small">
        <?php
        $term_id = get_primary_taxonomy_id(get_the_ID(), 'work-category');
        $pr_term = get_term($term_id);
        ?>
        <div class="page-category-name"><?php
            if (!empty($pr_term)) {
                echo $pr_term->name;
            }
            ?></div>
        <?php
        echo '<h2>';
        the_field('title', false, false);
        echo '</h2>';
        if (have_posts()) : while (have_posts()) : the_post();
                the_content();
            endwhile;
        endif;
        
        $awards = $post->post_excerpt;
        if(!empty($awards)){
        ?>	
        <div class="section-tag text-color-primary"><span class="icon-Awwwards icon"></span><?php echo $awards; ?></div>
        <?php } ?>
    </div>
    <div class="highlights-toggle">
        <div class="wrapper-highlighter">
            <span class="icon-Pen-1 icon "></span>
            <span class="icon-Pen-2 icon "><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></span>
            <span class="highlighter-txt">oxyMoron Highlighter</span>
        </div>
    </div>
    <?php
    if (have_rows('section')):
        while (have_rows('section')) : the_row();
            $image_id = get_sub_field('image');
            $image_mobile_id = get_sub_field('mobile_image');
            if(isMobile()){
	            $image_url = wp_get_attachment_image_src($image_mobile_id, 'full');
	        }else{
		        $image_url = wp_get_attachment_image_src($image_id, 'full');
	        }    
            
            if ($image_url) {
                ?>
                <div class="page-banner">
	                
	                <img src="<?php echo $image_url[0]; ?>" alt="">
                </div>
                <?php
            }
            //the_sub_field('content',false,false);
            echo apply_filters('the_content', get_sub_field('content', false));
        endwhile;
    endif;
    ?>
    <?php
        $quote = get_field('quote');
        if(!empty($quote)) {
    ?>
    <div class="section brand-words">
        <?php echo $quote; ?>
    </div>
    <?php } ?>
    <div class="section brand-services">
        <div class="service"></div>
        <?php
        if (have_rows('direction_fields')):
            while (have_rows('direction_fields')): the_row();
                ?>
                <div class="service">
                    <?php
                    the_sub_field('field_title', false, false);
                    if (have_rows('service_list')):
                        while (have_rows('service_list')): the_row();
                            echo '<p> -' . get_sub_field('service') . '</p>';
                        endwhile;
                    endif;
                    ?>
                </div>
                <?php
            endwhile;
        endif;
        ?>
        <div class="service"></div>
    </div>

    <?php
    $args = array(
        'post_type' => 'work',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );
    $postlist = get_posts($args);
    $pages = array();
    foreach ($postlist as $post) {
        $pages[] = $post->ID;
    }
    $current = array_search(get_the_ID(), $pages);
    $prevID = $pages[$current - 1];
    $nextID = isset($pages[$current + 1]) ? $pages[$current + 1] : $pages[0];
    if (empty($nextID)) {
        $nextID = $pages[0];
    }
    if (empty($prevID)) {
        $prevID = $pages[count($pages) - 1];
    }
    $prev_url = get_the_post_thumbnail_url($prevID);
    $next_url = get_the_post_thumbnail_url($nextID);
    ?>
    <div class="section next-previous-project">

        <div class="project" style="background-image: url(<?php echo $prev_url; ?>)">
            <a href="<?php echo get_the_permalink($prevID); ?>">
                <div class="overlay"></div>
                <div class="direction"><div class="upperline"></div>Previous Project</div>
                <div class="name text-color-primary"><?php echo get_the_title($prevID); ?></div>
            </a>
        </div>
        <div class="project" style="background-image: url(<?php echo $next_url; ?>)">
            <a href="<?php echo get_the_permalink($nextID); ?>">
                <div class="overlay"></div>
                <div class="direction"><div class="upperline"></div>Next Project</div>
                <div class="name text-color-primary"><?php echo get_the_title($nextID); ?></div>
            </a>
        </div>
    </div>
</div>

<?php
get_footer();
