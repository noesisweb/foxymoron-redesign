<?php /* Template Name: Team Landing Page*/ 
 get_header(); 

$team_details = get_field('team_member_details');
if (!empty($team_details)) {
    ?>
    <div class="page-team dragscroll">
        <div class="drag-content">
            <div class='foxy-grid-wrapper'>
                <?php
                foreach ($team_details as $team_member) {
                    $team_member_image = $team_member['team_member_image'];
                    $team_member_image_hover = $team_member['team_member_hover_image'];
                    ?>
                    <div class="foxy-grid-item">
					      <div class="image original" style="background-image: url(<?php echo $team_member_image['sizes']['team_listing'] ;?>)"></div>
				          <div class="image hover" style="background-image: url(<?php echo $team_member_image_hover['sizes']['team_listing'];?>)"></div>
				          <h3 class="name"><?php echo $team_member['team_member_name']; ?></h3>
				          <p><?php echo $team_member['team_member_designation']; ?></p>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}

?>
<?php get_footer(); ?>
