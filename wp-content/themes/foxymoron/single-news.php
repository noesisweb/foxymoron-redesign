<?php get_header(); 
	
	function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}
	if(isMobile()){
		$image_url =get_field('landing_page_image', get_the_ID());
	}else{
		$image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0];
	}
?>



<div class="page-news-inner" style="background-image:url(<?php echo $image_url?>)">
	<div class="section">
		<div class="content">
			<div class="heading"><div class="upperline"></div><?php echo get_field('formatted_post_title');?></div>
			<p><?php the_content();?></p>
		</div>
	</div>
</div>

<?php get_footer(); ?>