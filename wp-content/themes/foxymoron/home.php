<?php
/* Template Name: Home Page */
get_header();
$home_page_posts = get_field('home_page_posts');
if (!empty($home_page_posts)) {
    ?>
    <div class="page-home">
        <?php
        foreach ($home_page_posts as $vertical) {
            $vertical_title = $vertical['vertical_title'];
            $image = $vertical['image'];
            $vertical_green_color = '';
            if (!$image) {
                $vertical_green_color = 'text-color-primary';
            }
            ?>
            <div class="section-title">
                <div class="left"><div><?php echo $vertical_title; ?></div></div>
                <div class="right  <?php echo $vertical_green_color; ?>"><div><?php echo $vertical_title; ?></div></div>
            </div>
            <?php
        }
        ?>
        <div class="content">
            <div class="home-content-slider">
                <ul>
                    <?php
                    foreach ($home_page_posts as $content) {
                        $type = $content['type'];
                        $category = $content['category'];
                        $title = $content['title'];
                        $post_content = $content['content'];
                        $link = $content['link'];
                        if ($type != '') {
                            ?>
                            <li>
                                <div class="latest-tag">
                                    Latest
                                    <hr>
                                </div>
                                <div class="content-text">
                                    <div class="case-study-tag">
                                        <div class="upperline"></div>
                                        <?php echo $type; ?>
                                    </div>
                                    <?php echo $title; ?>
                                    <?php echo $post_content; ?>
                                    <div class="view"><a href="<?php echo $link; ?>"><span class="icon-Submit-Arrow"></span></a></div>
                                    <div class="case-study-note">
                                        <?php echo $category; ?>
                                    </div>
                                </div>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li class="active">
                                <div class="content-text">
                                    <?php echo $title; ?>
                                    <?php echo $post_content; ?>
            <!--                                     <div class="view"><a href="<?php echo $link; ?>"><span class="icon-Submit-Arrow"></span></a></div> -->
                                </div>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div id="fullpage">
            <?php
            foreach ($home_page_posts as $background) {
                $bg_color = $background['background_color'];
                $background_img = '';
                if ($background['image']) {
                    $bg_img = wp_get_attachment_image_url($background['image'], 'full');
                    $background_img = "background-image: url('" . $bg_img . "')";
                }
                ?>
                <div class="section" style="<?php echo $background_img; ?>"></div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
get_footer();
