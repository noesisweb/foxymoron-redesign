<?php /* Template Name: case study Page */ ?>
<?php get_header(); ?>


<div class="foxy-banner case-study-banner1">
	<div class="container">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				 
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/case-study-banner.jpg" alt="case-studys">
				</div>
				
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/case-study-banner.jpg" alt="case-studys">
				</div>
				
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/case-study-banner.jpg" alt="case-studys">
				</div>
					 
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>

<div class="container ">
<div class="case-study-description">
	
		
		<div class="case-study-logo">
			 <img src="<?php echo get_template_directory_uri() ?>/assets/images/case-study-logo.jpg" alt="case-studys">
		</div>
		<div class="case-study-text">
			<h1>Motorolla’s Biggest Phone Launch Ever - The Z<sup>2</sup> Play</h1>
			<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,
			   nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
		</div>
		
		<div class="case-study-text para1">
			<span>What We Did</span>
			<p><span>UI/UX Design</span><span>Web Development</span><span>Social Marketing</span><span>Influencer Outreach</span></p>
		</div>
		
		<div class="hairline"></div>
		
		<div class="case-study-text">
			<h3>The Challenge</h3>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit.</p>
		</div>
	
		<div class="case-study-text">
			<h3>Our Insight</h3>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
			   Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat,
			   velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
		</div>
		
	</div>
</div>

<div class="case-study-img">
	<div class="container">
	<img src="<?php echo get_template_directory_uri() ?>/assets/images/case-study1.jpg" alt="case-studys">
	</div>
</div>

<div class="container">
	<div class="case-study-description">
		<div class="case-study-text para1">
			<span>A world where every mod enabled #HelloPossibilities.</span>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
			   Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat,
			   velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p> 
		
			<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,
			   nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
			   Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat,
			   velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
		</div>
		
		<div class="case-study-text para1">
			<span>A website to redeem offers for the hardcore moto superfans.</span>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
			   Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, 				   velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p> 
		</div>
		
	</div>
</div>


<div class="foxy-banner case-study-banner2">
	<div class="container">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				 
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/offer-banner.jpg" alt="case-studys">
				</div>
				
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/offer-banner.jpg" alt="case-studys">
				</div>
				
				<div class="swiper-slide"> 
					<img src="<?php echo get_template_directory_uri() ?>/assets/images/offer-banner.jpg" alt="case-studys">
				</div>
					 
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>

<div class="container">
	<div class="case-study-description">
		<div class="case-study-text para1">
			<span>A community eagerly anticipating every update on Social Media.</span>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.
			   Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.
			   Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.</p>
		</div>
	</div>
</div>

<div class="foxy-banner case-study-banner3">
	<div class="container">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				 
				<div class="swiper-slide">
	
						<div class="col-md-4">
						<div class="fb-video" data-href="https://www.facebook.com/MotorolaIN/videos/1125238870845909/" data-show-text="true"><blockquote cite="https://www.facebook.com/MotorolaIN/videos/1125238870845909/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MotorolaIN/videos/1125238870845909/"></a><p>This #CrosswordPuzzleDay, take some time out for a better #phonelifebalance. Solve a real crossword, and show us how you fare!</p>Posted by <a href="https://www.facebook.com/MotorolaIN/">Motorola</a> on Thursday, December 21, 2017</blockquote></div>
						</div>
						<div class="col-md-4">
						<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BboH3f4Hngl/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8 ; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BboH3f4Hngl/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Chose your moto z mod? Only one:thinking_face:</a></p> <p style=" color:#c9c8cd ; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/motorola_/" style=" color:#c9c8cd ; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Motorola official</a> (@motorola_) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-11-18T06:42:15+00:00">Nov 17, 2017 at 10:42pm PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
						</div>
						<div class="col-md-4">
							<blockquote class="twitter-tweet" data-cards="hidden" data-lang="en"><p lang="en" dir="ltr">Avail great offers on the <a href="https://twitter.com/hashtag/motog?src=hash&amp;ref_src=twsrc%5Etfw">#motog</a> series, packed with a great design &amp; advanced cameras. <a href="https://twitter.com/amazonIN?ref_src=twsrc%5Etfw">@AmazonIN</a>! <a href="https://t.co/92UwKeGgIA">https://t.co/92UwKeGgIA</a></p>&mdash; Motorola India (@motorolaindia) <a href="https://twitter.com/motorolaindia/status/943292371101470720?ref_src=twsrc%5Etfw">December 20, 2017</a></blockquote>
							<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
				</div>
				<div class="swiper-slide">
	
						<div class="col-md-4">
						<div class="fb-video" data-href="https://www.facebook.com/MotorolaIN/videos/1125238870845909/" data-show-text="true"><blockquote cite="https://www.facebook.com/MotorolaIN/videos/1125238870845909/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/MotorolaIN/videos/1125238870845909/"></a><p>This #CrosswordPuzzleDay, take some time out for a better #phonelifebalance. Solve a real crossword, and show us how you fare!</p>Posted by <a href="https://www.facebook.com/MotorolaIN/">Motorola</a> on Thursday, December 21, 2017</blockquote></div>
						</div>
						<div class="col-md-4">
						<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BboH3f4Hngl/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8 ; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BboH3f4Hngl/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">Chose your moto z mod? Only one:thinking_face:</a></p> <p style=" color:#c9c8cd ; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/motorola_/" style=" color:#c9c8cd ; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> Motorola official</a> (@motorola_) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2017-11-18T06:42:15+00:00">Nov 17, 2017 at 10:42pm PST</time></p></div></blockquote> <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
						</div>
						<div class="col-md-4">
							<blockquote class="twitter-tweet" data-cards="hidden" data-lang="en"><p lang="en" dir="ltr">Avail great offers on the <a href="https://twitter.com/hashtag/motog?src=hash&amp;ref_src=twsrc%5Etfw">#motog</a> series, packed with a great design &amp; advanced cameras. <a href="https://twitter.com/amazonIN?ref_src=twsrc%5Etfw">@AmazonIN</a>! <a href="https://t.co/92UwKeGgIA">https://t.co/92UwKeGgIA</a></p>&mdash; Motorola India (@motorolaindia) <a href="https://twitter.com/motorolaindia/status/943292371101470720?ref_src=twsrc%5Etfw">December 20, 2017</a></blockquote>
							<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
						</div>
				</div>
				 
			</div>
			<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>


<div class="container ">
	<div class="case-study-description">
		<div class="case-study-text">
			<h3>The Result</h3>
			<p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
			   Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit.</p>

			<p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue.
			   Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque.
			   Suspendisse in orci enim.</p>
		</div>
		
		<div class="hairline"></div>
			
		<div class="case-study-text para1">
			<h3>Awards & Recognition</h3>
			<p><span>Gold - AFAQS Digital Awards 2017</span><span>2nd Place - Cannes Lions 2016</span><span>Special Mention - Digital Crest Awars 2017</span></p>
		</div>
	</div>
</div>


<div class="case-study-work">
	<div class="container">	
		<h2>Some Other Cool Work</h2>
		<div class="case-study-wrapper">
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs7.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs10.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch 2</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs9.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs8.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	        
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs3.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
		
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs6.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	
	        
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs5.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	
	        <div class="cb-block case-study-block">
	            <img src="<?php echo get_template_directory_uri() ?>/assets/images/cs2.jpg" alt="case-studys">
	            <div class="cs-info info">
	                <h3>NYX</h3>
	                <p>India Brand Launch</p>
	                <span class="fa fa-arrow-right arrow-right"></span>
	            </div>
	        </div>
	        	
		</div>	
	</div>
</div>


<?php get_footer();?>
