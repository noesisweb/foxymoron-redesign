<?php get_header(); ?>

<?php
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 3,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'News',
	);
	$post_args['meta_query'] = array(
	    'relation' => 'AND',
	    array(
	      'key'     => 'is_on_slide',
	      'value'   => 'yes',
	      'compare' => '=',
	    ),
	  );
	$featured_posts = new WP_Query( $post_args );
	
?>

<div class="foxy-banner news-banner">
	<div class="container">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<?php
				if($featured_posts->have_posts()) {
					while($featured_posts->have_posts()) {
					$featured_posts->the_post();
						
						if (has_post_thumbnail( get_the_ID()) ) {
							if(!wp_is_mobile()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'news_slider_desktop' );
							} else {
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_posts->ID ), 'news_slider_mobile' );
							}
						} else {
							$image = '';
						}
						
						$news_data = array();	
					    $news_data = getNewsData(get_the_ID());
	
						?>
						<div class="swiper-slide" style="background-image:url(<?php echo $image[0]; ?>)" >
							<div class="case-study-wrapper">
								<div class="cb-block news-block">
									<?php if($news_data['parent_cat']) {  ?>
									<div class="news-block-category"><?php echo $news_data['parent_cat'];?><span>//</span><?php if(!empty($news_data['primary_child_cat'])) echo $news_data['primary_child_cat']; else echo $news_data['child_category']; ?></div>
									<?php } else { ?>
										<div class="news-block-category"><?php echo $news_data['parent_cat']; ?></div>
									<?php } ?>
							        <h3><a href="<?php echo get_post_permalink(get_the_ID()); ?>"><?php the_title();?></a></h3>
							        <span class="fa fa-arrow-right arrow-right"></span>
							    </div>
							</div>
						</div>
						<?php
					}
				}
				wp_reset_query();
				?>
			</div>
		<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>

<div class="news-wrapper">
	<div class="container">	
		<div class="case-study-wrapper work-wrapper">
			<?php	
			if ( have_posts() ) {
				while ( have_posts() ) : the_post();
				
				$news_data = array();	
			    $news_data = getNewsData(get_the_ID());
				?>
				<a href="<?php echo get_permalink(get_the_ID()); ?>">
			        <div class="cb-block news-block">
						<?php if($news_data['parent_cat']) { ?>
						<div class="news-block-category"><?php echo $news_data['parent_cat']; ?><span>//</span><?php if(!empty($news_data['primary_child_cat'])) echo $news_data['primary_child_cat']; else echo $news_data['child_category']; ?></div>
						<?php } else { ?>
							<div class="news-block-category"><?php echo $news_data['parent_cat']; ?></div>
						<?php } ?>
			            <h3><?php the_title(); ?></h3>
			            <span class="fa fa-arrow-right arrow-right"></span>
			        </div>
				</a>
				<?php
					
		        endwhile;
	
			   if ( $wp_query->max_num_pages > 1 ) {
					echo '<div class="work_posts_pagination col-md-12">';
					the_posts_pagination( array(
						'mid_size'  => 2,
						'prev_text' => esc_html__( 'Previous', 'authentic' ),
						'next_text' => esc_html__( 'Next', 'authentic' ),
					) );
					echo '</div>';
				} 
		    }
	        ?>
		</div>	
	</div>
</div>

<?php get_footer();?>