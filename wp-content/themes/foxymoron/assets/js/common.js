var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) {
isMobile = true;
}

jQuery(document).ready(function () {
    dragscroll.reset();
    //jQuery('#form-apply').parsley();
    // Barba.Pjax.start();

    jQuery(".filter-list").click(function (event) {
        event.stopPropagation();
    });
    jQuery(".page-works .overlay").click(function () {
        jQuery("#worksFilter").css("width", "0");
        jQuery(this).css("display", "none");
    });
    jQuery(".filter-value").click(function () {
        jQuery("#worksFilter").css("width", "0");
        jQuery(".overlay").css("display", "none");
    });
    // END

    // append cropped items at bottom of page ( for team and clients page )
    var croppedItems = [];
    jQuery('.foxy-grid-item').each(function (i, obj) {
        var position = jQuery(this).position();
        if (position.top == 0) {
            if (i % 2 == 0) {
    //croppedItems.push(jQuery(this).clone());
            }
        }
    });
    jQuery.when(
            jQuery.each(croppedItems, function (i, obj) {
                jQuery('.foxy-grid-item:last-child').after(obj);
            })
            ).then(function () {
        jQuery('.foxy-grid-item:even').addClass('even');
        jQuery('.foxy-grid-item:odd').addClass('odd');
    });
    // END

    // highlight text in work inside page
    jQuery(".highlights-toggle").click(function () {
    //console.log("first");
    //jQuery( ".highlights-toggle" ).toggleClass("show");
    //jQuery( ".highlight" ).toggleClass("on");
        if (jQuery(".highlights-toggle").hasClass("show")) {
            jQuery(".highlights-toggle").removeClass("show");
            jQuery(".highlight").removeClass("on");
            jQuery(this).find(".wrapper-highlighter").animate({right: "-152px"}, 500);
        } else {
            jQuery(".highlights-toggle").addClass("show");
            jQuery(".highlight").addClass("on");
            jQuery(this).find(".wrapper-highlighter").animate({right: "10px"}, 500);
        }

        /*
         jQuery(this).find(".wrapper-highlighter").toggle(function(){
         jQuery( ".highlights-toggle" ).addClass("show");
         jQuery( ".highlight" ).addClass("on");
         jQuery(this).animate({right: "10px"},500);
         },function(){
         jQuery( ".highlights-toggle" ).removeClass("show");
         jQuery( ".highlight" ).removeClass("on");
         jQuery(this).animate({right: "-152px"},500);
         });
         */
    });
    // END


    // Home Page text swiper
    var pilingtexts_swiper = new Swiper('.pilingtexts-swiper', {
        direction: 'vertical',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
    // END

    // Home page background swipe
    jQuery(".page-home .section-title:nth-child(1)").addClass("active");
    jQuery('#fullpage').fullpage({
        easing: 'easeInOutCubic',
        autoScrolling: true,
        easingcss3: 'ease',
        fadingEffect: false,
        parallax: true,
        touchSensitivity: 25,
        lazyLoading: true,
        parallaxOptions: {type: 'cover', percentage: 100, property: 'translate'},
        onLeave: function (index, nextIndex, direction) {
            var bgcolor = '';
            var elm_exit = '';
            var elm_enter = '';
            bgcolor = jQuery(".page-home .section:nth-child(" + nextIndex + ")").data('bgcolor');
            jQuery(".page-home .content").css("background-color", bgcolor);
            elm_exit = jQuery(".page-home .section-title:nth-child(" + index + ")");
            elm_enter = jQuery(".page-home .section-title:nth-child(" + nextIndex + ")");
            elm_exit.removeClass("enter");
            elm_exit.addClass("exit");
            setTimeout(function () {
                elm_exit.removeClass("exit");
                elm_exit.removeClass("active");
                elm_enter.addClass("active");
                elm_enter.addClass("enter");
            }, 100);
            jQuery(".home-content-slider ul li").removeClass("active");
            jQuery(".home-content-slider ul li:nth-child(" + nextIndex + ")").addClass("active");
            jQuery(".home-content-slider ul").animate({
                top: -jQuery(".home-content-slider").height() * (nextIndex - 1)
            }, 500);
        },
        afterLoad: function (anchorLink, index) {
        },
        afterRender: function () {
            jQuery(".page-home .section-title:nth-child(1)").addClass("enter");
            bgcolor = jQuery(".page-home .section:nth-child(1)").data('bgcolor');
            jQuery(".page-home .content").css("background-color", bgcolor);
        },
        afterResize: function () {},
        afterResponsive: function (isResponsive) {},
        afterSlideLoad: function (anchorLink, index, slideAnchor, slideIndex) {
            var bgcolor = jQuery(".page-home .section.active").data('bgcolor');
            jQuery(".page-home .content").css("background-color", bgcolor);
        },
        onSlideLeave: function (anchorLink, index, slideIndex, direction, nextSlideIndex) {}
    });
    // END

    jQuery(".navigation-left").click(function () {
        var leftPos = jQuery('.horizontal-scroll-wrapper').scrollLeft();
        var width = jQuery('.article-content')[0].offsetWidth;
        console.log('width', width);
        jQuery(".horizontal-scroll-wrapper").animate({scrollLeft: leftPos - width}, 400);
    });
    jQuery(".navigation-right").click(function () {
        console.log('navigation-left');
        var leftPos = jQuery('.horizontal-scroll-wrapper').scrollLeft();
        var width = jQuery('.article-content')[0].offsetWidth;
        console.log('width', width);
        jQuery(".horizontal-scroll-wrapper").animate({scrollLeft: leftPos + width}, 600);
    });
    if (!isMobile) {
        var figure = jQuery(".single-block-wrapper").hover(hoverVideo, hideVideo);
    }

    if (isMobile) {
        jQuery('.single-block-wrapper video').attr("autoplay", true);
    }
    function hoverVideo(e) {
// 		console.log('called',this);
// 		console.log('called',jQuery('.horizontal-scroll-wrapper').children().not(this).addClass('non-hovered'));
//jQuery('.horizontal-scroll-wrapper', this).not(this).addClass('non-hovered');
        jQuery('video', this).get(0).play();
// 	    setTimeout(function(){  }, 3000);
// 	    console.log("test");

    }

    function hideVideo(e) {
        jQuery('video', this).get(0).pause();
        jQuery('.horizontal-scroll-wrapper').children().not(this).removeClass('non-hovered')
    }
// END

//Career page
    var url = window.location.href;
    jQuery('input:radio[name=careerDevisions]#city').click(function () {
        jQuery('.page-career .content-text').removeClass('active-block');
        jQuery('.page-career .content-text:nth-child(2)').addClass('active-block');
    });
    jQuery('input:radio[name=careerDevisions]#openings').click(function () {
        jQuery('.page-career .content-text').removeClass('active-block');
        jQuery('.page-career .content-text:nth-child(3)').addClass('active-block');
    });
    jQuery('.careerbyDivision input:radio').click(function () {
        jQuery('.count').css('opacity', '1');
        jQuery('.career-designations').removeClass('open');
        jQuery(this).siblings('label').find('.count').css('opacity', '0');
        jQuery(this).siblings('.career-designations').addClass('open');
    });
    jQuery('.career-designations li').click(function () {
        console.log(jQuery(this).data('career_id'));
        console.log(jQuery(this).data('career_title'));
        var career_id = jQuery(this).data('career_id');
        var career_title = jQuery(this).data('career_title');
        var career_location = jQuery(this).data('city');
        var career_desc = jQuery('#career_desc_' + career_id).html();
        jQuery('.career_details_section .career_title').html(career_title);
        jQuery('.career_details_section .career_desc').html(career_desc);
        jQuery('#input_8_11').val(career_title + ' - ' + career_location);
        jQuery('.page-career .content-text').removeClass('active-block');
        jQuery('.page-career .content-text:nth-child(4)').addClass('active-block');
        //window.location = url + '#apply';
    });
    //Input file Click Action
    jQuery('.input-file-style').click(function () {
        console.log(jQuery('.input-file-style input'));
        if (jQuery('.input-file-style input')[0] != undefined) {
            jQuery('.input-file-style input')[0]['disabled'] = true;
        }
        jQuery('.hidden-file-input input').trigger('click');
    });
    jQuery('.hidden-file-input input').change(function () {
        var filename = jQuery(this).val().split('\\').pop();
        console.log('filename', filename);
        jQuery('.input-file-style input').val(filename);
    });
    jQuery('.menu').click(function () {

        jQuery(this).toggleClass('active');
        jQuery('.menu-container').toggleClass('open');
        jQuery('.foxy-logo').toggleClass('shift-light');
        if (jQuery("body").hasClass("background-change") == true) {
            if (jQuery("body").hasClass("background-none") == true) {
                jQuery('body').addClass('background-light');
                jQuery('body').removeClass('background-none');
            } else {
                jQuery('body').addClass('background-none');
                jQuery('body').removeClass('background-light');
            }
        }

    });
    jQuery('.menu-item-has-children > a').click(function (event) {
        event.preventDefault();
        jQuery('.menu-item-has-children.menu-item').removeClass('active');
        //jQuery(this).next().addClass('active');
        //jQuery(this)[0]['parentElement'].addClass('active-element')
        console.log(jQuery(this).parent().addClass('active'));
    })
    jQuery(document).bind('gform_post_render', function () {

        jQuery('.input-file-style').click(function () {
            if (jQuery('.input-file-style input')[0] != undefined) {
                jQuery('.input-file-style input')[0]['disabled'] = true;
            }
            jQuery('.hidden-file-input input').trigger('click');
        });
        jQuery('.hidden-file-input input').change(function () {
            var filename = jQuery(this).val().split('\\').pop();
            console.log('filename', filename);
            jQuery('.input-file-style input').val(filename);
            //jQuery('#input_8_9').val('abc');
            //jQuery('.input-file-style input').text(filename);
        });
        if (jQuery(".hidden-file-input").hasClass("gfield_error")) {
            console.log('has class');
            jQuery('.input-file-style').addClass('gfield_error');
        }
    });
    // change foxymoron logo and menu on hover
    jQuery(".page-services .section")
            .mouseenter(function () {
                let position = jQuery(this).offset();
                //setInterval(function() { console.log(position); }, 200);
                if (position.left < 100) {
                    jQuery('body').addClass('services-hover');
                } else {
                    jQuery('body').removeClass('services-hover');
                }
            }).mouseleave(function () {
        jQuery('body').removeClass('services-hover');
    });
    // END

    //var number
    var servicesSwiper = new Swiper('.services-swiper-container', {
        direction: 'horizontal',
        slidesPerView: 2,
        mousewheel: true,
        speed: 1000,
        grabCursor: true,
        freeMode: true,
        breakpoints: {
            767: {
                slidesPerView: 1.3,
            }
        }

    });
    jQuery(".foxy-logo a").click(function (event) {
        jQuery(".foxy-logo").toggleClass('active')
    });
    // work landing page isotope configurations
    jQuery(".page-works").scrollLeft(300);
    var $grid = jQuery('.grid').isotope({
        itemSelector: '.item',
        layoutMode: 'fitRows'
    });
    var filterFns = {
        ium: function () {
            var name = jQuery(this).find('.name').text();
            return name.match(/ium$/);
        }
    };
    jQuery('.filters').on('click', 'input', function () {
        var filterValue = this.value;
        filterValue = filterFns[ filterValue ] || filterValue;
        $grid.isotope({filter: filterValue});
    });
    //media query
    //console.log('matchMedia');
    var worksFilterWidth;
    if (window.matchMedia('(max-width: 767px)').matches) {

        worksFilterWidth = "85%";
        jQuery(".page-services .section").click(function () {
            jQuery(".page-services .section").removeClass('hover');
            jQuery(this).addClass('hover');
        });
        //document.getElementById("services").addEventListener("wheel", onMouseWheel);
    } else {
        worksFilterWidth = "40%";
        $grid.on('arrangeComplete', function (event, filteredItems) {
            console.log('item length', filteredItems.length);
            jQuery(filteredItems).each(function (i, el) {
                el.element.classList.remove("odd");
                el.element.classList.remove("even");
                if (i % 2 === 0) {
                    el.element.className += ' even';
                } else {
                    el.element.className += ' odd';
                }
            });
        });
    }

// Filter wrapper hide and open function - work landing page
    jQuery("#worksFilter").click(function (event) {
        if (jQuery("#worksFilter").width() == 0) {
            jQuery("#worksFilter").css("width", worksFilterWidth);
            jQuery(".page-works .overlay").css("display", "block");
            //event.stopPropagation();
        }
    });
    jQuery(".horizontal-scroll-wrapper").mousewheel(function (event, delta) {

        this.scrollLeft -= (delta * 3);
        event.preventDefault();
    });
});
/*
 var scroller = {};
 scroller.e = document.getElementsByClassName("horizontal-scroll-wrapper");
 
 if (scroller.e.addEventListener) {
 scroller.e.addEventListener("mousewheel", MouseWheelHandler, false);
 scroller.e.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
 } else scroller.e.attachEvent("onmousewheel", MouseWheelHandler);
 
 function MouseWheelHandler(e) {
 
 // cross-browser wheel delta
 var e = window.event || e;
 var delta = - 20 * (Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))));
 
 var pst = $('.horizontal-scroll-wrapper').scrollLeft() + delta;
 
 if (pst < 0) {
 pst = 0;
 } else if (pst > $('.img_holder').width()) {
 pst = $('.img_holder').width();
 }
 
 $('.horizontal-scroll-wrapper').scrollLeft(pst);
 
 return false;
 }
 */


function changeCareerSlide(number) {
    jQuery('.page-career .content-text').removeClass('active-block');
    jQuery('.page-career .content-text:nth-child(' + number + ')').addClass('active-block');
}
function onMouseWheel() {
}
jQuery(window).load(function () {
    console.log("loading complete ");
    jQuery('#page-overlay').fadeOut();
});