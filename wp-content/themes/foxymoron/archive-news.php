<?php get_header(); 
    $post_count = 01;
?>
<div class="page-news-landing">
	<div class="horizontal-scroll-wrapper">
<!--
		<article class="single-block no-image">
			<div class="article-content">
				<div class="content">
					
					<div class="article-title"><div class="upperline"></div><p>Press</br>Release</p></div>
					<p class="article-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p class="article-list"><a href="<?php echo site_url(); ?>/news-category/pr-list/"><span>Archived PR list</span></a></p>
				</div>
			</div>
		</article>
-->
		<?php while (have_posts()) : the_post();
			$image_url = get_field('landing_page_image', get_the_ID());
			?>
			<article class="single-block">
				<div class="background-image image-1" style="background-image:url(<?php echo $image_url?>)">
					<div class="overlay"></div>
				</div>
				
				<div class="article-content">
					<div class="content">
						<div class="article-number">
						<p>
						<?php 
						if($post_count < 10) {
							echo '0'.+$post_count; 
							
						}else{
							echo $post_count;
						} ?>
						</p></div>
						<a href="<?php echo get_permalink() ?>">
							<?php echo 	get_field('formatted_post_title'); ?>
						</a>
					</div>
				</div>
			</article>
		<?php 
			$post_count ++;
			endwhile; 
		?>
	</div>
	<div class="horizontal-scroll-navigation">
		<button class="navigation navigation-left"><span class="fa fa-arrow-left"></span></button>
		<button class="navigation navigation-right"><span class="fa fa-arrow-right"></span></button>
	</div>
</div>

<?php get_footer(); ?>