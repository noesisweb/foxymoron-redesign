<?php /* Template Name: archived pr list */ ?>
<?php get_header(); ?>

<div class="page-pr-list">
	<div class="horizontal-scroll-wrapper">
		<article class="single-block no-image">
			<div class="article-content">
				<div class="content">
					<div class="article-title"><div class="upperline"></div><p>Archived</br>PR List</p></div>
					<p class="article-desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
					<p class="article-list"><span>PR Release</span></p>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>
				</div>
			</div>
		</article>	
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>				
				</div>
			</div>
		</article>
		<article class="single-block">
			<div class="article-content">
				<div class="content">
					<div class="content-inner">
						<div class="upperline"></div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
					</div>
				</div>
			</div>
		</article>	
	</div>
	<div class="horizontal-scroll-navigation">
		<button class="navigation navigation-left"><span class="fa fa-arrow-left"></span></button>
		<button class="navigation navigation-right"><span class="fa fa-arrow-right"></span></button>
	</div>
</div>

<?php
	get_footer();
?>