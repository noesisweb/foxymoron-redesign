<?php /* Template Name: News Page */ ?>
<?php get_header(); ?>

<?php
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$post_args = array(
		'post_type' 	=> 'post',
		'post_status'	=> 'publish',
		'posts_per_page'=> 4,
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		'category_name'	=> 'News',
		'paged' => $paged,
	);
	$featured_posts = new WP_Query( $post_args );	
?>

<div class="foxy-banner news-banner">
	<div class="container">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				<div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/news-banner.jpg)" >
					<div class="case-study-wrapper">
						<?php
							if($featured_posts->have_posts()) { 
								$featured_posts->the_post();
								
								$main_cat = get_the_category(get_the_ID());
								$child_cat = get_category($main_cat[0]->cat_ID);
								$parent_name = get_category($child_cat->parent);
								$parent_name = $parent_name->name;
						?>
						<div class="cb-block news-block">
							<?php if($parent_name) { ?>
							<div class="news-block-category"><?php echo $parent_name; ?><span>//</span><?php echo $main_cat[0]->name; ?></div>
							<?php } else { ?>
								<div class="news-block-category"><?php echo $main_cat[0]->name; ?></div>
							<?php } ?>
							<h3><?php the_title(); ?></h3>
							<p><?php echo get_field('short_description'); ?></p>
							<span class="fa fa-arrow-right arrow-right"></span>
						</div>
						<?php }  wp_reset_query();?>
	        		</div>
				</div>
				
				<div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/news-banner.jpg)" >
				</div>
				
				<div class="swiper-slide" style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/news-banner.jpg)">
				</div>
			
			</div>
		<!-- Add Pagination -->
			<div class="swiper-pagination"></div>
		</div>
	</div>
</div>



<div class="news-wrapper">
	<div class="container">	
		<div class="case-study-wrapper work-wrapper">
			<?php
			if($featured_posts->have_posts()) {
				while($featured_posts->have_posts()) {
				$featured_posts->the_post();
				$main_cat = get_the_category(get_the_ID());
				$child_cat = get_category($main_cat[0]->cat_ID);
				$parent_name = get_category($child_cat->parent);
				$parent_name = $parent_name->name;
				?>
				<a href="<?php echo get_permalink(get_the_ID()); ?>">
			        <div class="cb-block news-block">
						<?php if($parent_name) { ?>
						<div class="news-block-category"><?php echo $parent_name; ?><span>//</span><?php echo $main_cat[0]->name; ?></div>
						<?php } else { ?>
							<div class="news-block-category"><?php echo $main_cat[0]->name; ?></div>
						<?php } ?>
			            <h3><?php the_title(); ?></h3>
			            <span class="fa fa-arrow-right arrow-right"></span>
			        </div>
				</a>
				<?php
		        }
		        $total_pages = $featured_posts->max_num_pages;
			    if ($total_pages > 1) {
			        $current_page = max(1, get_query_var('paged'));
			        echo '<div class="work_posts_pagination col-md-12">';
			        echo paginate_links(array(
			            'base' => get_pagenum_link(1) . '%_%',
			            'format' => 'page/%#%',
			            'current' => $current_page,
			            'total' => $total_pages,
			            'prev_text'    => __('« prev'),
			            'next_text'    => __('next »'),
			        ));
			        echo '</div>';
			    } 
		    }
		    wp_reset_query();
	        ?>
		</div>	
	</div>
</div>

<?php get_footer();?>