<?php get_header(); ?>
<?php
$terms_category = get_terms( array(
    'taxonomy' => 'work-category',
    'hide_empty' => false,
) );	
$terms_service = get_terms( array(
    'taxonomy' => 'work-service',
    'hide_empty' => false,
) );
	
$post_args = array(
    'post_type' => 'work',
    'post_status' => 'publish',
    'order_by' => 'date',
    'order' => 'ASC',
    'posts_per_page' => '-1',
);
$work_posts = new WP_Query($post_args);
/*
echo '<pre>';
print_r($work_posts->posts);
echo '</pre>';
*/
?>
<div class="page-works">
	<div id="worksFilter" class="filter-wrapper">
		<div class="content">
			<div  class="filter-tabs">
				<div class="filter-tabs-wrapper">
					<ul class="nav nav-tabs">
				    	<li class="active"><a data-toggle="tab" href="#home">By Category</a></li>
						<li><a data-toggle="tab" href="#menu1">By Services</a></li>
				 	</ul>
				</div>
			</div>
			<div class="filter-list">
				<div class="tab-content">
				    <div id="home" class="tab-pane fade in active">
						<div class="filters">
								<label class="filter-value">
									<div class="brand-name">
										<div class="foxy-radio">
											<input type="radio" name="filter" id="all" value="*">
											<label for="all" class="radio-label">All</label>
										</div>
									</div>
									<div class="brand-count">Brands</div>
								</label>
							<?php foreach ($terms_category as $term) { ?>
								<label class="filter-value">
									<div class="brand-name">
										<div class="foxy-radio">
											<input type="radio" name="filter" id="<?php echo $term->slug ?>" value=".<?php echo $term->slug ?>">
											<label for="<?php echo $term->slug ?>" class="radio-label"><?php echo $term->name ?></label>
										</div>
									</div>
									<div class="brand-count"><?php echo $term->count ?> Brands</div>
								</label>
								
							<?php } ?>
						</div>
				    </div>
				    <div id="menu1" class="tab-pane fade">
						<div class="filters">
							<label class="filter-value">
								<div class="brand-name">
									<div class="foxy-radio">
										<input type="radio" name="filter" id="all" value="*">
										<label for="all" class="radio-label">All</label>
									</div>
								</div>
								<div class="brand-count">Brands</div>
							</label>
							<?php foreach ($terms_service as $term) { ?>
								<label class="filter-value">
									<div class="brand-name">
										<div class="foxy-radio">
											<input type="radio" name="filter" id="<?php echo $term->slug ?>" value=".<?php echo $term->slug ?>">
											<label for="<?php echo $term->slug ?>" class="radio-label"><?php echo $term->name ?></label>
										</div>
									</div>
									<div class="brand-count"><?php echo $term->count ?> Brands</div>
								</label>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="grid">
		<div class="overlay"></div>
			<div class="grid-masonary">
				<?php
					if($work_posts->have_posts()) {
		                while ($work_posts->have_posts()) {
		                    $work_posts->the_post();
		                    if (has_post_thumbnail(get_the_ID())) {
	                        	$image = wp_get_attachment_image_src(get_post_thumbnail_id($work_posts->ID), 'work_img');
		                    } else {
		                        $image = '';
		                    }
		                    $taxonomy_names = get_post_taxonomies(get_the_ID());
		                    $terms_slugs = array();
		                    if(!empty($taxonomy_names)){
			                    foreach ($taxonomy_names as $taxonomy) {
				                    $terms = get_the_terms(get_the_ID(), $taxonomy);
				                    if (!empty($terms)) {
					                    foreach ($terms as $term) {
						                    array_push($terms_slugs, $term->slug);
					                    }
				                    }
			                    }
		                    }
		                    $terms_slugs = implode (" ", $terms_slugs);
		                    ?>
		                    <div class="item transition <?php echo $terms_slugs; ?>" data-category="transition">
			                    <a href="<?php echo the_permalink(); ?>">
				                    <div class="brand-image" style="background-image: url(<?php if (!empty($image)) echo $image[0]; ?>)">
										<div class="brand-image-hover"><?php echo get_field('overlay_text'); ?></div>
									</div>
								    <h3 class="name"><?php the_title(); ?></h3>
								    <p><?php the_excerpt(); ?></p>
			                    </a>
							</div>
		                    
		                    <?php 
			                   
		                }
	                }
				?>	
			</div>
		</div>
</div>
<?php get_footer(); ?>
