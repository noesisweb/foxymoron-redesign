<?php 
	/* Template Name: news-landing-inner */ 
 	get_header();
?>
<div class="page-news-inner">
	<div class="section">
		<div class="content">
			<div class="heading"><span class="text-color-tertiary">About<br></span><span class="text-color-primary">FoxyMoron</span></div>
			<p>US fast-food giant Burger King has assigned its digital mandate to leading digital solutions agency FoxyMoron.The Mandate entails strategic planning and out of the box solutions focusedon building affinity via brand advocacy on digital.</p>
			<p>To find out more, read on at <a class="coloured" href="http://foxy.noesis.tech">ET Brand Equity.</a></p>
		</div>
	</div>
</div>
<?php
	get_footer();
?>