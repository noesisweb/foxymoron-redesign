<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package foxymoron
 */
 
$post = $wp_query->post;

if ( in_category( 'case-studies' ) ) {
  include( TEMPLATEPATH.'/single-case-studies.php' );
} 
else if( in_category( 'news' )){
  include( TEMPLATEPATH.'/single-inner_news1.php' );
}
else {
  get_header(); ?>

	<div id="primary" class="content-area container">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				//comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
}

