<?php
/* Template Name: Methodology */
get_header();
while (have_posts()) : the_post();
    $featured_image = get_post_thumbnail_url(get_the_ID(), 'full');
    $style = "background-image: url('" . $featured_image . "')";
    ?>
    <div class="page-aboutus" style="<?php echo $style; ?>">
        <div class="section">
            <div class="content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
    <?php
endwhile;
get_footer();
